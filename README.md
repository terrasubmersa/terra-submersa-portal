# Terra Submersa Portal

The project contains the core components of the terra-submersa... web portal.
The goal is to store and display various cartographic data, in the domain of archeological submersed landscapes.

## Launching the whole project
The project is powered by Docker. The various components are described in the *Architecture* section.

### For development

    cd components/ts-infra
    
Tune the environment (ports, env name etc.)

    . env_dev.sh
    
Run
    
    docker-compose up --build

### For production
Thanks to [`COMPOSE_PROJECT_NAME`](https://docs.docker.com/compose/reference/envvars/), the same `docker-compose.yml` can be ran multiple time in the same host.
Well, for the moment, we also need to suffix containers name per env for reverse proxiing.

Nonetheless, it is pretty straightforward. Do as above, with:
   
    . env_prod.sh

### Build and deployment
Continuous Integration is powered by gitlab-ci 

## Managing data 
### From command lines

Everything goes through the backend tiles api (for the moment). 

The rest API can be hit directly, but it might prove to be easier via our CLI tool in python:
    
    pip install terra-submersa-cli
    
Check the usage with 
    ts-tiles --help

Backend host are (`$TS_BACKEND`):

   * public prod: `TS_BACKEND=https://terra-submersa.demo.octo.ch/tiles`
   * locally dockerized (dev): `TS_BACKEND=http://localhost:84/tiles` 
   * locally launched: `TS_BACKEND=http://localhost:9000`

#### Checkout sample data 

    cd ..
    git clone git@gitlab.com:terra-submersa/data-kilada.git
    cd data-kilada
    
#### Data interaction

##### Tile systems
####### List 

    ts-tiles list $TS_BACKEND 
    ts-tiles list $TS_BACKEND --output=id
    
    
###### Posting data
####### png + params

An image can be posted to the backend based on an image and parameters (coordinates etc...)

Some data can be found in `git@gitlab.com:terra-submersa/data-kilada.git`, under the `misc` directory.

Beside each `.png` file resides a `.properties` file. This later file contains parameters. Those are pushed via the `ts-tile add` command.

    for $png in *.png
    do
      echo $png
	   props=$(echo $f | sed 's%.png%.properties%')
      ts-tiles add $TS_BACKEND $png $props 
    done

####### SENSYS

Sensys data comes with a `.png` + a `tif.txt` description file where the coordinates and other information can be extracted.
In fact, the original SENSYS come in a tif format, but they were process with imagemagick ton convert them to png and turn the white color into transparent.

Some data can be found in `git@gitlab.com:terra-submersa/data-kilada.git`, under the `sensys` directory.

    cd sensys
    for f in *.png
    do 
      descr=$(echo $f | sed 's%.png%%')
      ts-tiles add-sensys $TS_BACKEND \
               $f \
               $g.tif.txt \
               --title="$g" \
               --description="SENSYS data were produced by the GeoSat ReSearchlaboratory (Rethymno, Greece) in 2016, within the Kiladha Bay. \
               --copyright="Terra Submersa"
    done
    
###### Remove data

Let's remove all the tile systems where the ids contains `'_system_test_'`

    for id in $(ts-tiles list $TS_BACKEND --output=id)
    do
      echo "deleting $id"
      ts-tiles remove $TS_BACKEND id
    done
   
##### Text Annotations

###### list
    curl -s $TS_BACKEND/api/text-annotations | jq -c .[]

###### add one 

    curl -X POST -H "Content-Type: application/json" \
    -d '{"text":"paf le chien", "position": {"lat":37.4269307, "lon":23.1331932}, "tileSystemId": "sensys-JULY_18_A", "labels":["lab one"]}' \
    http://localhost:9000/api/text-annotation

###### removing all annotation for one system
    
    system_id="ts-id"
    
    for id in $(curl http://localhost:9000/api/text-annotations | jq -r '.[] | select(.tileSystemId == "'$system_id'") | .id')
    do 
      echo "removing ($id)"
      curl -X DELETE $TS_BACKEND/api/text-annotation/$id
    done

## Project Architecture
### Technical components

  * a JavaScript (Angular) `ts-frontend` to display and interact with cartographic content
  * a Scala + Play backend  `ts-tile-backend` to serve map tile systems (what are the available artifacts) and tile images (png files)
  * a mongodb backend to store stuff

### Versioning strategy

The project version is described in the root `PROJECT_VERSION.txt` file.


## Authors
  * Julien Beck, University of Geneva, Switzerland
  * Philippe Kernevez, OCTO Technology, Switzerland
  * Alexandre Masselot, OCTO Technology Switzerland

## License