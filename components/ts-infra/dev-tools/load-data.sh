#!/usr/bin/env bash

## Push metadata to existing image (system-legacy)
remote_ts="$1"
echo "remote ($remote_ts)"
if [ "$remote_ts" == "" ]
then
   echo "pass the backend host as first argument"
   echo "  $0 https://terra-submersa.demo.octo.ch/tiles"
   echo "  $0 http://localhost:84/tiles"
   echo "  $0 http://localhost:9000"
   exit 1
fi

curl -d '{
    "id": "terra.submersa.images.heli11",
    "boundaries": {
      "minLevel": 12,
      "maxLevel": 19,
      "bbox": {
        "upperLeft": {
          "lat": 37.46029356319137,
          "lon": 23.09417842005971
        },
        "lowerRight": {
          "lat": 37.39003123767094,
          "lon": 23.14084352618228
        }
      }
    },
    "urlMask": "/images/heli11/{z}/{x}/{y}.png",
    "description": "bathymetry color map in Kilada Bay",
    "copyright": "Terra Submersa"
  }' -H "Content-Type: application/json" -X POST --location $remote_ts/api/system-legacy

curl -d '{
    "id": "terra.submersa.contours.heli11",
    "boundaries": {
      "minLevel": 12,
      "maxLevel": 19,
       "bbox": {
        "upperLeft": {
          "lat": 37.46029356319137,
          "lon": 23.09417842005971
        },
        "lowerRight": {
          "lat": 37.39003123767094,
          "lon": 23.14084352618228
        }
      }
    },
    "urlMask": "/contours/heli11/{z}/{x}/{y}.png",
    "description": "bathymetry contour curves in Kilada Bay",
    "copyright": "Terra Submersa"
  }' -H "Content-Type: application/json" -X POST --location $remote_ts/api/system-legacy
