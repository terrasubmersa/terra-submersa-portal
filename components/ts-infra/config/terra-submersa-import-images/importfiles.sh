#!/usr/bin/env bash

#docker run -it --network tsdev_terra-submersa -v ~/.ssh:/root/.ssh bravissimolabs/alpine-git bash

eval $(ssh-agent -s)
ssh-add <(echo "$CI_GIT_PRIVATE_KEY")

mkdir /temp
cd /temp
#git clone --depth=1
git clone git@gitlab.com:terra-submersa/data-kilada.git .
cd sensys

export TS_BACKEND=http://tsdev_terra_submersa_nginx_reverse_proxy_1/tiles

## Wait for backend, max 20s
duration=0
until $(curl --output /dev/null --silent --head --fail $TS_BACKEND); do
    printf '.'
    duration=$((duration+5))
    echo $duration
    if [[ $duration -gt 19 ]]; then
        echo "No server started..."
        exit 1
    fi
    sleep 5
done

## Purge existing images
## tile ids has a sensys- prefix
for id in $(curl $TS_BACKEND/api/systems | jq . | grep '"id": "' | cut -f4 -d\" | grep "sensys-")
do
  echo "deleting $id"
  curl -X DELETE $TS_BACKEND/api/system/$id | jq .
done

## TODO PKE : erreur après import
# play.api.UnexpectedException: Unexpected exception[NoSuchFileException: /data/tiles/osm-tiles/images/sensys-july11-12/11/1155/794.png]
#terra_submersa_tile_backend_1         | 	at play.api.http.HttpErrorHandlerExceptions$.throwableToUsefulException(HttpErrorHandler.scala:251)
#terra_submersa_tile_backend_1         | 	at play.api.http.DefaultHttpErrorHandler.onServerError(HttpErrorHandler.scala:182)
#terra_submersa_tile_backend_1         | 	at play.core.server.AkkaHttpServer$$anonfun$$nestedInanonfun$executeHandler$1$1.applyOrElse(AkkaHttpServer.scala:251)
#terra_submersa_tile_backend_1         | 	at play.core.server.AkkaHttpServer$$anonfun$$nestedInanonfun$executeHandler$1$1.applyOrElse(AkkaHttpServer.scala:250)
#terra_submersa_tile_backend_1         | 	at scala.concurrent.Future.$anonfun$recoverWith$1(Future.scala:412)
#terra_submersa_tile_backend_1         | 	at scala.concurrent.impl.Promise.$anonfun$transformWith$1(Promise.scala:37)
#terra_submersa_tile_backend_1         | 	at scala.concurrent.impl.CallbackRunnable.run(Promise.scala:60)
#terra_submersa_tile_backend_1         | 	at play.api.libs.streams.Execution$trampoline$.execute(Execution.scala:70)
#terra_submersa_tile_backend_1         | 	at scala.concurrent.impl.CallbackRunnable.executeWithValue(Promise.scala:68)
#terra_submersa_tile_backend_1         | 	at scala.concurrent.impl.Promise$KeptPromise$Kept.onComplete(Promise.scala:368)

## Import des nouvelles images
for f in *.png
do
  g=$(echo $f | sed 's%.png%%')
  curl  -F "file_image=@$f" -F "file_txt=@$g.tif.txt" -F \
    "text_title=$g" -F "text_description=SENSYS data were produced by the GeoSat ReSearchlaboratory (Rethymno, Greece) in 2016, within the Kiladha Bay." \
     -F "text_copyright=Terra Submersa"   $TS_BACKEND/api/sensys2d | jq .
done

echo "Fin import des images"