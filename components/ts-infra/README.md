#Terra Submersa infrastructure

##Launch the docker-compose

The `docker-compose.yml` is parametrized by environment variables.
By default, those variable are set in `.env`.

You can set up the `prod|dev` environment via 

    . env_prod.sh 
    . env_dev.sh 

### In development mode
We append the special `docker-compose-dev-local.yml` file.
The goal is to target the deployment for development.
For example, opens up a port for the tile backend (`terra_submersa_tile_backend`), in order to have your Angular development directly hitting the dockerized backend    

    docker-compose -f docker-compose.yml -f docker-compose-dev-local.yml up