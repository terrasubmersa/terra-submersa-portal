#!/usr/bin/env bash

export DOCKER_TS_ENV_NAME=dev
export DOCKER_TS_PORT_WEB=84
export DOCKER_TS_VERSION=$(cat ../../PROJECT_VERSION.txt)
export COMPOSE_PROJECT_NAME=ts_dev
