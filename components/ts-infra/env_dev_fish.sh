#!/usr/bin/env fish 

set -x DOCKER_TS_ENV_NAME dev
set -x DOCKER_TS_PORT_WEB 84
set -x DOCKER_TS_VERSION (cat ../../PROJECT_VERSION.txt)
set -x COMPOSE_PROJECT_NAME ts_dev
