## Play

### Data interaction


#### Tile systems
##### pushing legacy tile system description
We just push the description, not the actual tiles files

    curl -d '{
      "id":"abc",
      "boundaries":{"minLevel":12,"maxLevel":19,"bbox":{"upperLeft" : {"lat":12.34, "lon":56.78 } ,"lowerRight" : {"lat":-12.34, "lon":156.78 } } },
      "urlMask":"http://aaa/{z}/{x}/{y}",
      "description": "WTF",
      "copyright": "paf"
    }' -H "Content-Type: application/json" -X POST -v http://localhost:9000/system-legacy
    
    
##### Removing a tile

    curl -X DELETE $TS_BACKEND/system/abc
    
#### 

### Local build with Docker
## Build


## Test

## Sonar... todi


### With build local installation
`sbt 013.16` is the version used for development
#### run
With hot update

    sbt "~run -Dconfig.file=conf/application-dev.conf"
    
*NB*: Maybe you need to modify/save one file to get rid of the `"file not watched"` message (registered sbt/play bug) 

#### test

    sbt ~test
    
### Package
#### Dev
Build an image 

    export VERSION=$(cat ../../PROJECT_VERSION.txt)
    export PROJECT_NAME=$(basename $(pwd))

    sbt docker:publishLocal
    
    docker tag $PROJECT_NAME:$VERSION registry.gitlab.com/terrasubmersa/terra-submersa-portal/$PROJECT_NAME:$VERSION
    docker tag registry.gitlab.com/terrasubmersa/terra-submersa-portal/$PROJECT_NAME:$VERSION registry.gitlab.com/terrasubmersa/terra-submersa-portal/$PROJECT_NAME:latest
    

## Tools

To view a .asc file in the terminal, zoom it out to the maximum and enter:

    cat  ~/Downloads/koilada_portoheli/koilada_utm34n.asc | sed 's/-9999 / /g' | perl -pe 's/\-\d*([0-9]+)(?:,\d+)? /$1/g' |cut -c1-1600


## Updating Mongo database

it's a bit the mess. We shall certainly incorporate something in the serivce, but let's do that by hand for the moment

    mongo
    use tiles
    # or use dev_tiles on dev

#### 2017.11.30
Adding a cmulsory title fields

    db.tile_system.find({title:null}).forEach(function(doc){doc.title = doc.id; db.tile_system.save(doc)})

#### 2017.12.19
Removing the useless images|contours base directory on the urlMask (thus the filepath)

    db.tile_system.find().forEach(function(doc){doc.urlMask=doc.urlMask.replace(/(images|contours)\//, ''); db.tile_system.save(doc)})

##### Special treatment for legacy helli11
They were only discriminated based on the based directory

    db.tile_system.find({id:'terra.submersa.images.heli11'}).forEach(function(doc){doc.urlMask=doc.urlMask.replace('/heli11/', '/heli11-images/'); db.tile_system.save(doc)})
    db.tile_system.find({id:'terra.submersa.contours.heli11'}).forEach(function(doc){doc.urlMask=doc.urlMask.replace('/heli11/', '/heli11-contours/'); db.tile_system.save(doc)})

##### moving the files around
cd to the tile data directory (look at application.conf if you are lost

And we shall start with our two heli11 special cases

    mv images/heli11 heli11-images
    mv contours/heli11 heli11-contours

    mv images/* .
    mv contours/* .

    rmdir images
    rmdir contours