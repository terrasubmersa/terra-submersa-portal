package ch.octo.terrasubmersa

package object annotations {

  case class TextAnnotationId(value: String)
  case class Label(value: String)

}
