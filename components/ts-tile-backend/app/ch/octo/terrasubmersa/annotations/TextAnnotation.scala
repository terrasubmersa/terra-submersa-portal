package ch.octo.terrasubmersa.annotations

import ch.octo.terrasubmersa.coordinates.LatLonCoords
import ch.octo.terrasubmersa.tiles.models.TileSystemId

case class TextAnnotation(id: Option[TextAnnotationId],
                          text: String,
                          position: LatLonCoords,
                          tileSystemId: TileSystemId,
                          labels: Set[Label]
                         ) {
  def setId(id: TextAnnotationId): TextAnnotation =
    TextAnnotation(
      Some(id),
      text,
      position,
      tileSystemId,
      labels
    )
}
