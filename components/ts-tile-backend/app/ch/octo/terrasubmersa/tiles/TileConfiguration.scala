package ch.octo.terrasubmersa.tiles

import java.io.File
import javax.inject._

import ch.octo.terrasubmersa.tiles.models.TileSystemId
import play.api.{Configuration, Logger}

@Singleton
class TileConfiguration @Inject()(config: Configuration) {
  val logger = Logger(this.getClass.getName)

  val tileFilePath = config.get[String]("tiles.files.path")

  def tileSystemdirectory(id: TileSystemId): File = {
    new File(tileFilePath + "/osm-tiles/"  + id.value)
  }
}
