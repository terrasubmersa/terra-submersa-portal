package ch.octo.terrasubmersa.tiles.slippy

import ch.octo.terrasubmersa.coordinates.HasLatLonBBox

/**
  * Created by alex on 23.08.17.
  */
case class SlippyTileGraph(root: SlippyTileNode) {

}

object SlippyTileGraph{
  def generateTree(root: SlippyTile, maxZoom: Int, filterTile: (HasLatLonBBox) => Boolean = (h) => true): SlippyTileGraph = {
    SlippyTileGraph(SlippyTileNode.generateTree(root, maxZoom, filterTile))
  }
}
