package ch.octo.terrasubmersa.tiles.slippy

import java.io.File

import ch.octo.terrasubmersa.coordinates.{HasLatLonBBox, LatLonBBox, LatLonCoords}

import scala.math._

/**
  * Created by alex on 21.08.17.
  */
case class SlippyTile(x: Int, y: Int, z: Int) extends HasLatLonBBox {

  def toCoords: LatLonCoords = {
    LatLonCoords(
      toDegrees(atan(sinh(Pi * (1.0 - 2.0 * y.toDouble / (1 << z))))),
      x.toDouble / (1 << z) * 360.0 - 180.0
    )
  }

  def toDir(parent: String):File = new File(s"$parent/$z/$x")

  def toDir(parent: File):File = toDir(parent.getAbsolutePath)

  def toFile(parent: String):File = new File(s"${toDir(parent)}/$y.png")

  def toFile(parent: File):File = toFile(parent.getAbsolutePath)

  lazy val toBBox: LatLonBBox = {
    LatLonBBox(
      toCoords,
      SlippyTile(x + 1, y + 1, z).toCoords
    )
  }

  def subTiles: Set[SlippyTile] =
    Set(
      SlippyTile(x << 1, y << 1, z + 1),
      SlippyTile((x << 1) + 1, y << 1, z + 1),
      SlippyTile(x << 1, (y << 1) + 1, z + 1),
      SlippyTile((x << 1) + 1, (y << 1) + 1, z + 1)
    )

}

object SlippyTile {
  val MIN_LEVEL = 0
  val MAX_LEVEL = 21
}