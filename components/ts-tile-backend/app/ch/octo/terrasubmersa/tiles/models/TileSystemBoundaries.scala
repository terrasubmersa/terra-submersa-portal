package ch.octo.terrasubmersa.tiles.models

import ch.octo.terrasubmersa.coordinates.LatLonBBox

case class TileSystemBoundaries(minLevel:Int, maxLevel:Int, bbox: LatLonBBox) {

}
