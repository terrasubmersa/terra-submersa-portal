package ch.octo.terrasubmersa.tiles

package object models {
  case class TileSystemId(value:String) extends AnyVal
}
