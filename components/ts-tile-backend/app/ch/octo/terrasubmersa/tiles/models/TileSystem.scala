package ch.octo.terrasubmersa.tiles.models

case class TileSystem(id: TileSystemId,
                      boundaries: TileSystemBoundaries,
                      urlMask: String,
                      title:String,
                      description: String,
                      copyright: String) {

}
