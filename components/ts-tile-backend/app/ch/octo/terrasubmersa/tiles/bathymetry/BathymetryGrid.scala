package ch.octo.terrasubmersa.tiles.bathymetry

import ch.octo.terrasubmersa.bathymetry.BathymetryCell
import ch.octo.terrasubmersa.coordinates.LatLonBBox

/**
  * Created by alex on 09.09.17.
  */
case class BathymetryGrid(nrows:Int, ncols:Int, boundaries: LatLonBBox, cells: List[List[BathymetryCell]]) {
  lazy val  maxDepth = cells.flatten.filter(_.oVa.isDefined).map(_.oVa.get).max
  lazy val minDepth = cells.flatten.filter(_.oVa.isDefined).map(_.oVa.get).min
}
