package ch.octo.terrasubmersa.tiles.services

import ch.octo.terrasubmersa.tiles.models.TileSystem
import play.api.Logger
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.bson.BSONDocument

/**
  * write and reads data to/from MongoDB database
  * connection parameters are defined in the application.conf
  *
  */
object TileMongoService extends MongoService[TileSystem] {
  val logger: Logger = Logger(this.getClass())

  import BSONSerializer._


  def bsonReader = implicitly

  def bsonWriter = implicitly

  val mongoCollectionName = "tile_system"
  val index = Some(Index(
    Seq("id" -> IndexType.Ascending), unique = true)
  )

  def idSelector(id: String): BSONDocument = BSONDocument("id" ->id)
}