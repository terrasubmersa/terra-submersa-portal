package ch.octo.terrasubmersa.tiles.services

import ch.octo.terrasubmersa.annotations.{TextAnnotation, TextAnnotationId}
import ch.octo.terrasubmersa.tiles.models.TileSystemId
import play.api.Logger

import scala.concurrent.ExecutionContext.Implicits.global
import reactivemongo.bson.{BSONDocument, BSONObjectID, BSONString}

import scala.concurrent.Future

/**
  * write and reads data to/from MongoDB database
  * connection parameters are defined in the application.conf
  *
  */
object TextAnnotationMongoService extends MongoService[TextAnnotation] {
  val logger: Logger = Logger(this.getClass())

  import BSONSerializer._

  def bsonReader = implicitly

  def bsonWriter = implicitly

  val mongoCollectionName = "text_annotation"
  val index = None

  def idSelector(id: String) = {
    val q = BSONDocument("_id" -> BSONObjectID.parse(id).get)
    q
  }

  /**
    * Add or update a document (depending if an id is provided
    *
    * @param textAnnotation
    * @return
    */
  override def insert(textAnnotation: TextAnnotation): Future[TextAnnotation] = {
    val id = BSONObjectID.generate()
    val taWithId = textAnnotation.setId(TextAnnotationId(id.stringify))
    super.insert(taWithId)
      .map(_ => taWithId)
  }

  def findAllByTileSystemId(id: TileSystemId): Future[List[TextAnnotation]] = {
    val q = BSONDocument("tileSystemId" -> BSONString(id.value))
    findAll(q)
  }
  def removeAllByTileSystemId(id: TileSystemId): Future[Int] = {
    val q = BSONDocument("tileSystemId" -> BSONString(id.value))
    removeAll(q)
  }

}