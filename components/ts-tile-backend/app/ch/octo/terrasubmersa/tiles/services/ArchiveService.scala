package ch.octo.terrasubmersa.tiles.services

import java.io.File
import javax.inject.{Inject, Singleton}

import ch.octo.terrasubmersa.annotations.TextAnnotation
import ch.octo.terrasubmersa.tiles.TileConfiguration
import ch.octo.terrasubmersa.tiles.controllers.JsonTransformers._
import ch.octo.terrasubmersa.tiles.models.{TileSystem, TileSystemId}
import org.apache.commons.io.FileUtils
import org.zeroturnaround.zip.{ByteSource, FileSource, ZipEntrySource, ZipUtil}
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc.ControllerComponents

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.io.Source


/**
  * compress and explode tile system infor to/from a zio archive
  */
@Singleton
class ArchiveService @Inject()(cc: ControllerComponents,
                               config: Configuration,
                               tileConfiguration: TileConfiguration
                              ) extends TilerService(tileConfiguration) {


  /**
    * build a zip archive for a tile system, with
    * * config.json the configuration dump
    * * tiles: all the tiles *.png
    * * text-annotations
    *
    * @param id
    * @param outZip
    * @return
    */
  def zipIt(id: TileSystemId, outZip: File): Future[Unit] = {

    TileMongoService.findOne(id.value)
      .map(_ match {
        case Some(ts) =>
          val tileDirname = tileConfiguration.tileSystemdirectory(id).getAbsolutePath
          FileUtils.listFiles(tileConfiguration.tileSystemdirectory(id), List("png").toArray, true)
            .asScala
            .map((f) => {
              new FileSource(f.getAbsolutePath.replace(tileDirname, "tiles"), f)
            })
            .toList
            .:+(new ByteSource("config.json", Json.prettyPrint(Json.toJson(ts)).getBytes()))
        case None => Future.failed(ObjectNotFoundInStore("tile system", id.value))
      })
      .flatMap({ case (sources: List[ZipEntrySource]) => {
        TextAnnotationMongoService.findAllByTileSystemId(id)
          .map((tas) => {
            val tasSources = tas.zipWithIndex.map({
              case (ta: TextAnnotation, i: Int) =>
                new ByteSource(s"text-annotations/$i.json", Json.prettyPrint(Json.toJson(ta)).getBytes())
            })
            sources ::: tasSources
          })
      }
      })
      .map((sources: List[ZipEntrySource]) =>
        ZipUtil.pack(sources.toArray, outZip)
      )
  }

  def unzipIt(archiveFile: File): Future[TileSystem] = {
    val tmpDir = File.createTempFile("unzipIt-", "-" + archiveFile.getName)
    tmpDir.delete()
    tmpDir.mkdir()

    def getTmpFile(filename: String) = new File(tmpDir.getAbsolutePath + "/" + filename)

    ZipUtil.unpack(archiveFile, tmpDir)
    val json = Source.fromFile(getTmpFile("config.json")).getLines().mkString("\n")
    val tileSystem = Json.fromJson[TileSystem](Json.parse(json)).get

    TileMongoService.insert(tileSystem)
      .map((ts) => {
        // copy all file
        FileUtils.copyDirectory(getTmpFile("tiles"), tileConfiguration.tileSystemdirectory(ts.id))
        ts
      })
      .flatMap((ts) => {
        if (!getTmpFile("text-annotations").exists()) {
          Future {
            ts
          }
        } else {
          // insert jsoned text annotations
          val listFutureTextAnnotationsInsert = FileUtils.listFiles(getTmpFile("text-annotations"), List("json").toArray, true)
            .asScala
            .toList
            .map((taFile) => {
              val json = Source.fromFile(taFile).getLines().mkString("\n")
              val ta = Json.fromJson[TextAnnotation](Json.parse(json)).get
              TextAnnotationMongoService.insert(ta)
            })
          val futureListTextAnnotationsInsert = Future.sequence(listFutureTextAnnotationsInsert)
          futureListTextAnnotationsInsert.map((_) => ts)
        }
      })
  }
}
