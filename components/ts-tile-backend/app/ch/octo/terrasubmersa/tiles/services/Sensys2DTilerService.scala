package ch.octo.terrasubmersa.tiles.services

import java.io.File
import javax.inject.{Inject, Singleton}

import ch.octo.terrasubmersa.slippy.ZoomRange
import ch.octo.terrasubmersa.tiles.TileConfiguration
import ch.octo.terrasubmersa.tiles.io.reader.SensysReader
import ch.octo.terrasubmersa.tiles.models.{TileSystem, TileSystemBoundaries, TileSystemId}
import ch.octo.terrasubmersa.tiles.transformers.ImageTiler
import play.api.Configuration
import play.api.mvc.ControllerComponents

import scala.concurrent.{ExecutionContext, Future}


/**
  * Break a Sensys 2D image into tiles and stores it in a database
  */
@Singleton
class Sensys2DTilerService @Inject()(cc: ControllerComponents,
                                     config: Configuration,
                                     tileConfiguration: TileConfiguration
                                    ) extends TilerService(tileConfiguration) {


  /** tiles the image and save into the Mongo database
    *
    * @param id
    * @param inImageFile
    * @param inTxtFile
    * @param descriptionText an optional String. If not present, a description is extracted from the sensys .txt file
    * @param copyright
    * @param zoom
    * @param ec
    * @return
    */
  def image2tilesAndSave(id: TileSystemId,
                         inImageFile: File,
                         inTxtFile: File,
                         title:String,
                         descriptionText: Option[String],
                         copyright: String,
                         zoom:ZoomRange

                        )(implicit ec: ExecutionContext): Future[TileSystem] = {
    def boundaries = SensysReader.readBoundingBox(inTxtFile)

    val actualDescription = descriptionText.getOrElse(SensysReader.readDescription(inTxtFile))

    image2tilesAndSave(id, inImageFile, boundaries, title, actualDescription, copyright, zoom)
  }
}
