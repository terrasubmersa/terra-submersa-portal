package ch.octo.terrasubmersa.tiles.services

import java.io.File
import javax.inject.{Inject, Singleton}

import ch.octo.terrasubmersa.coordinates.LatLonBBox
import ch.octo.terrasubmersa.slippy.ZoomRange
import ch.octo.terrasubmersa.tiles.TileConfiguration
import ch.octo.terrasubmersa.tiles.io.reader.SensysReader
import ch.octo.terrasubmersa.tiles.models.{TileSystem, TileSystemBoundaries, TileSystemId}
import ch.octo.terrasubmersa.tiles.transformers.ImageTiler
import play.api.Configuration
import play.api.mvc.ControllerComponents

import scala.concurrent.{ExecutionContext, Future}


/**
  * Break a Sensys 2D image into tiles and stores it in a database
  */
abstract class TilerService(
                             tileConfiguration: TileConfiguration
                           ) {

  /**
    * fomr a full configuration, writes down the image tiles
    */

  def image2tiles(id: TileSystemId,
                  inImageFile: File,
                  boundaries: LatLonBBox,
                  title: String,
                  description: String,
                  copyright: String,
                  zoom:ZoomRange
                 )(implicit ec: ExecutionContext): Future[TileSystem] = {

    val outDir = tileConfiguration.tileSystemdirectory(id)

    ImageTiler.tile(inImageFile, boundaries, outDir, zoom)
      .map(_ => {
        TileSystem(
          id,
          TileSystemBoundaries(
            zoom.min.getOrElse(1),
            zoom.max,
            boundaries
          ),
          id.value + "/{z}/{x}/{y}.png",
          title,
          description,
          copyright
        )
      })
  }

  def image2tilesAndSave(id: TileSystemId,
                         inImageFile: File,
                         boundaries: LatLonBBox,
                         title:String,
                         description: String,
                         copyright: String,
                         zoom:ZoomRange

                        )(implicit ec: ExecutionContext): Future[TileSystem] = {

    image2tiles(id, inImageFile, boundaries, title, description, copyright, zoom)
      .flatMap(tileSystem => {

        TileMongoService.insert(tileSystem)
          .map(_ => tileSystem)
      })
  }

}
