package ch.octo.terrasubmersa.tiles.services

import com.typesafe.config.ConfigFactory
import play.api.Logger
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.commands.bson.DefaultBSONCommandError
import reactivemongo.api.indexes.Index
import reactivemongo.api.{Cursor, MongoConnection, MongoDriver, ReadPreference}
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

case class ObjectNotFoundInStore(collection: String, id: String) extends Exception(s"object not found in [$collection] with id [$id]")

/**
  * @TODO wire back connection if the contact was lost
  *
  */
trait MongoService[T] {
  val logger: Logger

  implicit def bsonReader: BSONDocumentReader[T]

  implicit def bsonWriter: BSONDocumentWriter[T]

  def mongoCollectionName: String

  def index: Option[Index]

  def idSelector(id: String): BSONDocument

  val mongoUri = ConfigFactory.load.getString("mongodb.uri")
  val mongoDatabaseName = ConfigFactory.load.getString("mongodb.databaseName")

  lazy val connection: Future[MongoConnection] = {
    logger.info(s"connecting to mongo $mongoUri, database=$mongoDatabaseName, collection=$mongoCollectionName")
    val driver = MongoDriver()
    val parsedUri = MongoConnection.parseURI(mongoUri)
    Future.fromTry(parsedUri.map(driver.connection(_)))
  }

  /**
    * get the collection.
    * and eventually create indexes
    */
  lazy val mongoCollection: Future[BSONCollection] = {
    connection
      .flatMap((c: MongoConnection) =>
        c.database(mongoDatabaseName)
          .map(x =>
            x.collection(mongoCollectionName)
          )
      )
      .flatMap((collection: BSONCollection) => setupCollection(collection))

  }

  /**
    * Setup indexes for the tile system collection
    *
    * @param collection
    * @return
    */
  def setupCollection(collection: BSONCollection): Future[BSONCollection] = {
    (collection.create().
      map { _ => collection }
      recover {
      //with the magic 48 number??? https://github.com/mongodb/mongo/blob/master/src/mongo/base/error_codes.err
      //NamespaceExists
      case e: DefaultBSONCommandError if e.code == Some(48) =>
        collection
    }
      )
      .flatMap(_ => {
        index match {
          case None => Future {
            collection
          }
          case Some(idx) => collection.indexesManager.ensure(idx)
            .map(_ => collection)

        }
      })
  }

  /**
    * add  or update (if an id field os provided) into the the collection
    *
    * @param document
    * @return
    */
  def insert(document: T): Future[T] = {
    val writeRes = mongoCollection.flatMap(c => c.insert(document))
    writeRes.onComplete {
      case Failure(e) => throw e
      case Success(writeResult) =>
        println(s"successfully inserted document: $writeResult")
    }
    writeRes.map(r => document)
  }

  /**
    * add  or update (if an id field os provided) into the the collection
    *
    * @param document
    * @return
    */
  def update(id: String, document: T): Future[T] = {
    val writeRes = mongoCollection.flatMap(c => c.update(idSelector(id), document))
    writeRes.onComplete {
      case Failure(e) => throw e
      case Success(writeResult) =>
        println(s"successfully inserted document: $writeResult")
    }
    writeRes.map(r => document)
  }

  /**
    * retrieves a single document based on is id
    *
    * @param id
    * @return
    */
  def findOne(id: String): Future[Option[T]] = {
    mongoCollection.flatMap(c => c.find(idSelector(id)).one[T])
  }

  /**
    * retrieves a single document based on is id
    *
    * @param selector
    * @return
    */
  def findAll(selector: BSONDocument): Future[List[T]] = {
    mongoCollection.flatMap(c => c.find(selector)
      .cursor[T]()
      .collect(-1, Cursor.FailOnError[Vector[T]]())
    )
      .map(_.toList)
  }


  /**
    * add a TileSystem into the
    *
    * @param id the Tile id
    * @return
    */
  def removeDocument(id: String): Future[String] = {
    val selector = idSelector(id)
    removeAll(selector)
      .map((i) => i match {
        case 0 => throw ObjectNotFoundInStore(mongoCollectionName, selector.toString())
        case _ => id
      })
  }

  def removeAll(selector: BSONDocument): Future[Int] = {
    val futureRemove = mongoCollection.flatMap(c => c.remove(selector))
    futureRemove.map((writeResults) => writeResults.n)
  }

  /**
    *
    * @return
    */
  def listDocuments: Future[List[T]] = {
    val query = BSONDocument()
    mongoCollection
      .flatMap(_.find(query)
        .cursor[T](ReadPreference.primaryPreferred)
        .collect[List](Int.MaxValue, Cursor.FailOnError[List[T]]())
      )
  }
}