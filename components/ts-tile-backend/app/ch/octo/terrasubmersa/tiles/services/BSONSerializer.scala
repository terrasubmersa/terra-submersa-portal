package ch.octo.terrasubmersa.tiles.services

import ch.octo.terrasubmersa.annotations.{Label, TextAnnotation, TextAnnotationId}
import ch.octo.terrasubmersa.coordinates.{LatLonBBox, LatLonCoords}
import ch.octo.terrasubmersa.tiles.models.{TileSystem, TileSystemBoundaries, TileSystemId}
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter, BSONNull, BSONObjectID, BSONReader, BSONString, BSONValue, BSONWriter, Macros}

object BSONSerializer {

  implicit def LatLonCoordsWriter: BSONDocumentWriter[LatLonCoords] = Macros.writer[LatLonCoords]

  implicit def LatLonCoordsReader: BSONDocumentReader[LatLonCoords] = Macros.reader[LatLonCoords]

  implicit def LatLonBBoxWriter: BSONDocumentWriter[LatLonBBox] = Macros.writer[LatLonBBox]

  implicit def LatLonBBoxReader: BSONDocumentReader[LatLonBBox] = Macros.reader[LatLonBBox]

  implicit def TileSystemBoundariesWriter: BSONDocumentWriter[TileSystemBoundaries] = Macros.writer[TileSystemBoundaries]

  implicit def TileSystemBoundariesReader: BSONDocumentReader[TileSystemBoundaries] = Macros.reader[TileSystemBoundaries]

  implicit object TileSystemIdReader extends BSONReader[BSONString, TileSystemId] {
    def read(bson: BSONString): TileSystemId =
      new TileSystemId(bson.as[String])
  }
  implicit object TileSystemIdWriter extends BSONWriter[TileSystemId, BSONString] {
    def write(score: TileSystemId): BSONString = BSONString(score.value)
  }

  implicit def TileSystemWriter: BSONDocumentWriter[TileSystem] = Macros.writer[TileSystem]

  implicit def TileSystemReader: BSONDocumentReader[TileSystem] = Macros.reader[TileSystem]

  implicit object LabelReader extends BSONReader[BSONString, Label] {
    def read(bson: BSONString): Label =
      new Label(bson.as[String])
  }
  implicit object LabelIdWriter extends BSONWriter[Label, BSONString] {
    def write(label: Label): BSONString = BSONString(label.value)
  }

  implicit def TextAnnotationIdWriter: BSONDocumentWriter[TextAnnotationId] = Macros.writer[TextAnnotationId]

  implicit def TextAnnotationIdReader: BSONDocumentReader[TextAnnotationId] = Macros.reader[TextAnnotationId]

  /**
    * explicitly written to take care of _id (instead of id) and not messing the original  class
    * with some reactive mongo @Key annotation
    */
  implicit object TextAnnotationWriter extends BSONDocumentWriter[TextAnnotation] {
    def write(t: TextAnnotation) = {
      BSONDocument(
        "_id" -> t.id.map(id => BSONObjectID(id.value)).getOrElse(BSONNull),
        "text" -> t.text,
        "position" -> t.position,
        "tileSystemId" -> t.tileSystemId,
        "labels" -> t.labels
      )
    }
  }

  implicit object TextAnnotationReader extends BSONDocumentReader[TextAnnotation] {
    override def read(doc: BSONDocument): TextAnnotation = {
      TextAnnotation(
        doc.getAs[BSONObjectID]("_id").map(id => TextAnnotationId(id.stringify)),
        doc.getAs[String]("text").get,
        doc.getAs[LatLonCoords]("position").get,
        doc.getAs[TileSystemId]("tileSystemId").get,
        doc.getAs[Set[Label]]("labels").get
      )
    }
  }

}
