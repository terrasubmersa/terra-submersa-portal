package ch.octo.terrasubmersa.tiles.services

import java.io.File
import javax.inject.{Inject, Singleton}

import ch.octo.terrasubmersa.slippy.ZoomRange
import ch.octo.terrasubmersa.tiles.TileConfiguration
import ch.octo.terrasubmersa.tiles.io.reader.SensysReader
import ch.octo.terrasubmersa.tiles.models.{TileSystem, TileSystemId}
import play.api.Configuration
import play.api.mvc.ControllerComponents

import scala.concurrent.{ExecutionContext, Future}


/**
  * Break a Sensys 2D image into tiles and stores it in a database
  */
@Singleton
class WithCoordinatesTilerService @Inject()(cc: ControllerComponents,
                                            config: Configuration,
                                            tileConfiguration: TileConfiguration
                                    ) extends TilerService(tileConfiguration) {

}
