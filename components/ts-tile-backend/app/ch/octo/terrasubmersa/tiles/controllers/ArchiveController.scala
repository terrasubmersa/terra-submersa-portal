package ch.octo.terrasubmersa.tiles.controllers

import java.io.File
import javax.inject._

import ch.octo.terrasubmersa.tiles.TileConfiguration
import ch.octo.terrasubmersa.tiles.models.TileSystemId
import ch.octo.terrasubmersa.tiles.services.ArchiveService
import play.api.libs.json.Json
import play.api.mvc._
import play.api.{Configuration, Logger}

import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class ArchiveController @Inject()(cc: ControllerComponents,
                                  zipService: ArchiveService,
                                  config: Configuration,
                                  tileConfiguration: TileConfiguration
                                 ) extends AbstractController(cc) {


  val logger: Logger = Logger(this.getClass())

  def zip(id: String) = Action.async {
    val ftmp = File.createTempFile("zip-tile-system-" + id + "-", ".zip")
    zipService.zipIt(TileSystemId(id), ftmp)
      .map(_ => {
        Ok.sendFile(
          content = ftmp,
          inline = true,
          fileName = (_) => s"tile-system-$id.zip",
          onClose = () => ftmp.delete()
        )
      })
  }

  def unzip = Action.async(parse.multipartFormData) { implicit request =>
    val (zipFilename, zipFile) = ControllerRequestUtils.requestFile("archive")
    zipService.unzipIt(zipFile)
      .map((tileSystem) =>
        Ok(Json.obj(
          "status" -> "OK",
          "message" -> s"unziped archive id=${tileSystem.id.value}"
        ))
      )
  }
}

