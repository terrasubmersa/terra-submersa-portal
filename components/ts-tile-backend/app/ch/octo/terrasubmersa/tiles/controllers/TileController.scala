package ch.octo.terrasubmersa.tiles.controllers

import java.io.File
import java.nio.file.Paths
import javax.inject._

import ch.octo.terrasubmersa.coordinates.{CoordinateSystem, LatLonBBox, LatLonCoords}
import ch.octo.terrasubmersa.slippy.ZoomRange
import ch.octo.terrasubmersa.tiles.TileConfiguration
import ch.octo.terrasubmersa.tiles.controllers.JsonTransformers._
import ch.octo.terrasubmersa.tiles.models.{TileSystem, TileSystemId}
import ch.octo.terrasubmersa.tiles.services.{Sensys2DTilerService, TextAnnotationMongoService, TileMongoService, WithCoordinatesTilerService}
import org.apache.commons.io.FileUtils
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.Files
import play.api.libs.Files.TemporaryFile
import play.api.libs.json.{JsError, Json}
import play.api.mvc._
import play.api.{Configuration, Logger}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class TileController @Inject()(cc: ControllerComponents,
                               sensys2DTilerService: Sensys2DTilerService,
                               withCoordinatesTilerService: WithCoordinatesTilerService,
                               config: Configuration,
                               tileConfiguration: TileConfiguration
                              ) extends AbstractController(cc) {


  val logger: Logger = Logger(this.getClass())

  /**
    * get the list of TileSystem descriptions
    */
  def systemsList = Action.async {
    TileMongoService.listDocuments.map(x => Json.toJson(x))
      .map(l => Ok(l))
  }


  /**
    * Just posting a tile system description (with the data files already existing)
    *
    * @return
    */
  def addLegacySystem = Action.async(parse.json) { request =>
    val placeResult = request.body.validate[TileSystem]
    placeResult.fold(
      errors => {
        Future.successful(BadRequest(Json.obj("status" -> "KO", "message" -> JsError.toJson(errors))))
      },
      tileSystem => {
        TileMongoService.insert(tileSystem)
          .map(_ => Ok(Json.obj("status" -> "OK", "message" -> ("system '" + tileSystem.id + "' saved."))))
      }
    )
  }


  case class PngDataWithCoordinatesData(
                                         id: Option[String],
                                         coordinateSystem: String,
                                         latMin: String,
                                         latMax: String,
                                         lonMin: String,
                                         lonMax: String,
                                         title: Option[String],
                                         textDescription: Option[String],
                                         copyright: Option[String]
                                       )

  /**
    * get a post / multipart requst to insert a tile system
    *
    * @return
    */
  def addWithCoordinates = Action.async(parse.multipartFormData) { implicit request =>
    val (imageFilename, imageFile) = ControllerRequestUtils.requestFile("file_image")
    //val (descriptionFilename, descriptionFile) = localFile("file_txt")

    //val (descriptionFilename, descriptionFile) = localFile("file_txt")
    val pngDataWithCoordinatesForm = Form(
      mapping(
        "id" -> optional(text),
        "coordinate_system" -> text,
        "lat_min" -> text,
        "lat_max" -> text,
        "lon_min" -> text,
        "lon_max" -> text,
        "text_title" -> optional(text),
        "text_description" -> optional(text),
        "text_copyright" -> optional(text)
      )(PngDataWithCoordinatesData.apply)(PngDataWithCoordinatesData.unapply)
    )

    pngDataWithCoordinatesForm.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        val err = formWithErrors.errors.map(err => err.key -> err.messages).toMap
        Future {
          BadRequest(Json.obj(
            "status" -> "ERROR",
            "message" -> ("form validation " + (err.keys.mkString(","))),
            "details" -> err
          ))
        }
      },
      pdwcData => {
        /* binding success, you get the actual value. */
        val coordinateSystem = CoordinateSystem.withName(pdwcData.coordinateSystem)
        val latMinStr = pdwcData.latMin
        val latMaxStr = pdwcData.latMax
        val lonMinStr = pdwcData.lonMin
        val lonMaxStr = pdwcData.lonMax


        val boundaries =
          LatLonBBox(LatLonCoords.fromString(
            latMaxStr, lonMinStr, coordinateSystem),
            LatLonCoords.fromString(latMinStr, lonMaxStr, coordinateSystem)
          )

        val title: String = pdwcData.title
          .getOrElse(imageFilename.replace(".png", ""))

        val descriptionText = pdwcData.textDescription.getOrElse(imageFilename)

        val copyright: String = pdwcData.copyright.getOrElse("Terra Submersa")

        val id = TileSystemId(pdwcData.id.getOrElse(TileController.filename2id(imageFilename)))

        withCoordinatesTilerService.image2tilesAndSave(
          id,
          imageFile,
          boundaries,
          title,
          descriptionText,
          copyright,
          ZoomRange(Some(12), 19)
        ).map(tileSystem => Ok(Json.toJson(tileSystem)))

      }
    )
  }

  /**
    * Add a sensys data file + descrpition
    *
    * @return
    */
  def addSensys2d = Action.async(parse.multipartFormData) { implicit request =>

    val (imageFilename, imageFile) = ControllerRequestUtils.requestFile("file_image")
    val (descriptionFilename, descriptionFile) = ControllerRequestUtils.requestFile("file_txt")

    val title: String = request.body.dataParts.get("text_title")
      .map(s => s.mkString("\n"))
      .getOrElse(imageFilename.replace(".png", ""))
    val descriptionText: Option[String] = request.body.dataParts.get("text_description")
      .map(s => s.mkString("\n"))

    val copyright: String = request.body.dataParts.get("text_copyright")
      .map(s => s.mkString("\n"))
      .getOrElse("Terra Submersa")

    val id = TileSystemId(ControllerRequestUtils.requestData("id").getOrElse("sensys-" + TileController.filename2id(imageFilename)))

    sensys2DTilerService.image2tilesAndSave(
      id,
      imageFile,
      descriptionFile,
      title,
      descriptionText,
      copyright,
      ZoomRange(Some(12), 19)
    )
      .map(tileSystem => Ok(Json.toJson(tileSystem)))
  }

  /**
    * removes a tile system
    *
    * @param id
    * @return
    */
  def removeSystem(id: String) = Action.async { request =>
    TileMongoService.removeDocument(id)
      .map((id) => {
        val dir = tileConfiguration.tileSystemdirectory(TileSystemId(id))
        logger.info(s"removing directory ${dir.getAbsolutePath}")
        FileUtils.deleteDirectory(dir)
      })
      .flatMap(_ => {
        TextAnnotationMongoService.removeAllByTileSystemId(TileSystemId(id))
      })
      .map(_ => Ok(Json.obj(
        "status" -> "OK",
        "message" -> s"removed id $id"
      )))
  }

  /**
    * serve files
    *
    * @param path
    * @return
    */
  def serveData(path: String) = Action {
    val localPah = tileConfiguration.tileFilePath + "/" + path
    val file = new File(localPah)
    if (file.exists()) {
      Ok.sendFile(
        content = new File(localPah)
      )
    }
    else {
      logger.warn(s"no file $path")
      NoContent
    }
  }
}

object TileController {
  /**
    * return the filename without the file extension
    *
    * @param filename
    * @return
    */
  protected[controllers] def filename2id(filename: String): String = {
    filename
      .reverse
      .replaceFirst(".*?\\.", "")
      .reverse
  }

}