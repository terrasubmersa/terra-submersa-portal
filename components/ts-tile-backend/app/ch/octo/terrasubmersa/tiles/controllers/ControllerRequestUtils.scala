package ch.octo.terrasubmersa.tiles.controllers

import java.nio.file.Paths

import play.api.libs.Files
import play.api.libs.Files.TemporaryFile
import play.api.mvc.{MultipartFormData, Request}

object ControllerRequestUtils {
  /*
     get a file from post/multipart andd save it as a temporary file
    */
  def requestFile(name: String)(implicit request: Request[MultipartFormData[Files.TemporaryFile]]): (String, TemporaryFile) = request.body.file(name)
    .map(f => {
      val filename = Paths.get(f.filename).getFileName
      (filename.toString, f.ref)
    })
    .get

  /*
    get the field with a given name.
    If multiple are present, concatenate with \n
   */
  def requestData(name: String)(implicit request: Request[MultipartFormData[Files.TemporaryFile]]): Option[String] =
    request.body.dataParts.get(name).map(_.mkString("\n"))

}
