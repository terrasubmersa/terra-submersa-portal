package ch.octo.terrasubmersa.tiles.controllers

import javax.inject._

import ch.octo.terrasubmersa.annotations.TextAnnotation
import ch.octo.terrasubmersa.tiles.services.TextAnnotationMongoService
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{AbstractController, ControllerComponents}
import play.api.{Configuration, Logger}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import ch.octo.terrasubmersa.tiles.controllers.JsonTransformers._

import scala.util.parsing.json.JSONObject

@Singleton
class TextAnnotationController @Inject()(cc: ControllerComponents,
                                         config: Configuration
                                        ) extends AbstractController(cc) {
  val logger: Logger = Logger(this.getClass())

  def list = Action.async {
    TextAnnotationMongoService.listDocuments.map(x => Json.toJson(x))
      .map(l => Ok(l))
  }

  def add = Action.async(parse.json) { implicit request =>
    val placeResult = request.body.validate[TextAnnotation]
    placeResult.fold(
      errors => {
        Future.successful(BadRequest(Json.obj("status" -> "KO", "message" -> JsError.toJson(errors))))
      },
      annot => {
        TextAnnotationMongoService.insert(annot)
          .map(ta => Ok(Json.toJson(ta)))
      }
    )
  }

  def update(id: String) = Action.async(parse.json) { implicit request =>
    val placeResult = request.body.validate[TextAnnotation]
    placeResult.fold(
      errors => {
        Future.successful(BadRequest(Json.obj("status" -> "KO", "message" -> JsError.toJson(errors))))
      },
      annot => {
        TextAnnotationMongoService.update(id, annot)
          .map(ta => Ok(Json.toJson(ta)))
      }
    )
  }

  def remove(id: String) = Action.async { implicit request =>
    TextAnnotationMongoService.removeDocument(id)
      .map(_ => Ok(Json.obj(
        "status" -> "OK",
        "message" -> s"remove $id",
        "id" -> id
      )))
  }

}
