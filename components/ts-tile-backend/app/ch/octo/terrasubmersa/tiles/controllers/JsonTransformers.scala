package ch.octo.terrasubmersa.tiles.controllers

import ch.octo.terrasubmersa.annotations.{Label, TextAnnotation, TextAnnotationId}
import ch.octo.terrasubmersa.coordinates.{LatLonBBox, LatLonCoords}
import ch.octo.terrasubmersa.tiles.models.{TileSystem, TileSystemBoundaries, TileSystemId}
import play.api.libs.json._
import play.api.libs.functional.syntax._
import sun.font.Decoration

object JsonTransformers {
  val LatLonCoordsReads: Reads[LatLonCoords] = (
    (JsPath \ "lat").read[Double] and
      (JsPath \ "lon").read[Double]

    ) (LatLonCoords.apply _)

  val LatLonCoordsWrites: Writes[LatLonCoords] = (
    (JsPath \ "lat").write[Double] and
      (JsPath \ "lon").write[Double]
    ) (unlift(LatLonCoords.unapply))

  implicit val LatLonCoordsFormat: Format[LatLonCoords] =
    Format(LatLonCoordsReads, LatLonCoordsWrites)

  val LatLonBBoxReads: Reads[LatLonBBox] = (
    (JsPath \ "upperLeft").read[LatLonCoords] and
      (JsPath \ "lowerRight").read[LatLonCoords]

    ) (LatLonBBox.apply _)

  val LatLonBBoxWrites: Writes[LatLonBBox] = (
    (JsPath \ "upperLeft").write[LatLonCoords] and
      (JsPath \ "lowerRight").write[LatLonCoords]
    ) (unlift(LatLonBBox.unapply))

  implicit val LatLonBBoxFormat: Format[LatLonBBox] =
    Format(LatLonBBoxReads, LatLonBBoxWrites)


  val TileSystemBoundariesReads: Reads[TileSystemBoundaries] = (
    (JsPath \ "minLevel").read[Int] and
      (JsPath \ "maxLevel").read[Int] and
      (JsPath \ "bbox").read[LatLonBBox]

    ) (TileSystemBoundaries.apply _)

  val TileSystemBoundariesWrites: Writes[TileSystemBoundaries] = (
    (JsPath \ "minLevel").write[Int] and
      (JsPath \ "maxLevel").write[Int] and
      (JsPath \ "bbox").write[LatLonBBox]
    ) (unlift(TileSystemBoundaries.unapply))

  implicit val TileSystemBoundariesFormat: Format[TileSystemBoundaries] =
    Format(TileSystemBoundariesReads, TileSystemBoundariesWrites)

  val TileSystemIdReads: Reads[TileSystemId] = {
    (JsPath.read[String]).map(s => TileSystemId(s))
  }

  val TileSystemIdWrites: Writes[TileSystemId] = new Writes[TileSystemId] {
    def writes(l: TileSystemId) = JsString(l.value)
  }

  implicit val TileSystemIdFormat: Format[TileSystemId] =
    Format(TileSystemIdReads, TileSystemIdWrites)


  val TileSystemReads: Reads[TileSystem] = (
    (__ \ "id").read[TileSystemId] and
      (__ \ "boundaries").read[TileSystemBoundaries] and
      (__ \ "urlMask").read[String] and
      (__ \ "title").read[String] and
      (__ \ "description").read[String] and
      (__ \ "copyright").read[String]

    ) (TileSystem.apply _)

  val TileSystemWrites: Writes[TileSystem] = (
    (__ \ "id").write[TileSystemId] and
      (__ \ "boundaries").write[TileSystemBoundaries] and
      (__ \ "urlMask").write[String] and
      (__ \ "title").write[String] and
      (__ \ "description").write[String] and
      (__ \ "copyright").write[String]
    ) (unlift(TileSystem.unapply))

  implicit val TileSystemFormat: Format[TileSystem] =
    Format(TileSystemReads, TileSystemWrites)

  val LabelReads: Reads[Label] = {
    (JsPath.read[String]).map(s => Label(s))
  }

  val LabelWrites: Writes[Label] = new Writes[Label] {
    def writes(l: Label) = JsString(l.value)
  }

  implicit val LabelFormat: Format[Label] =
    Format(LabelReads, LabelWrites)

  val TextAnnotationIdReads: Reads[TextAnnotationId] = {
    (JsPath.read[String]).map(s => TextAnnotationId(s))
  }
  val TextAnnotationIdWrites: Writes[TextAnnotationId] = new Writes[TextAnnotationId] {
    def writes(l: TextAnnotationId) = JsString(l.value)
  }

  implicit val TextAnnotationIdFormat: Format[TextAnnotationId] =
    Format(TextAnnotationIdReads, TextAnnotationIdWrites)
  
  val TextAnnotationReads: Reads[TextAnnotation] = (
    (__ \ "id").readNullable[TextAnnotationId] and
      (__ \ "text").read[String] and
      (__ \ "position").read[LatLonCoords] and
      (__ \ "tileSystemId").read[TileSystemId] and
      (__ \ "labels").read[Set[Label]]
    ) (TextAnnotation)

  val TextAnnotationWrites: Writes[TextAnnotation] = (
    (__ \ "id").write[Option[TextAnnotationId]] and
    (JsPath \ "text").write[String] and
      (JsPath \ "position").write[LatLonCoords] and
      (JsPath \ "tileSystemId").write[TileSystemId] and
      (JsPath \ "labels").write[Set[Label]]
    ) (unlift(TextAnnotation.unapply))

  implicit val TextAnnotationFormat: Format[TextAnnotation] =
    Format(TextAnnotationReads, TextAnnotationWrites)
}