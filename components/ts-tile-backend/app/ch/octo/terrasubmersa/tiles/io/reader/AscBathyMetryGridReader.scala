package ch.octo.terrasubmersa.tiles.io.reader

import ch.octo.terrasubmersa.bathymetry.BathymetryCell
import ch.octo.terrasubmersa.coordinates.{CoordinateConverter, LatLonBBox}
import ch.octo.terrasubmersa.tiles.bathymetry.BathymetryGrid

import scala.io.Source

/**
  * Read text .asc file with bathymetry data and UTM64-34N coordiantes system
  */
class AscBathyMetryGridReader(boundaries: LatLonBBox) {

}

object AscBathyMetryGridReader {

  case class HeaderNotFoundValueException(value: String) extends Exception(s"header field [$value] was not found in the header")

  /*
  ncols         2775
  nrows         5406
  xllcorner     685864,77486308
  yllcorner     4141222,5066288
  cellsize      1,080643284309
  NODATA_value  -9999
   */
  val mRegexp = Map(
    "ncols" ->"""ncols[ ]+([0-9]+)""".r,
    "nrows" ->"""nrows\s+(\d+)""".r,
    "xllcorner" ->"""xllcorner\s+(\d+(,\d+)?)""".r,
    "yllcorner" ->"""yllcorner\s+(\d+(,\d+)?)""".r,
    "cellsize" ->"""cellsize\s+(\d+(,\d+)?)""".r
  )

  def load(filename: String) = {
    val reHeader = "[a-zA-Z].*"
    val header = Source.fromFile(filename).getLines().takeWhile(l => l.matches(reHeader)).toList.mkString("\n")

    val mHeaderVal = mRegexp.map(e => {
      val v = e._2.findFirstMatchIn(header) match {
        case Some(s) => s.group(1)
        case None => throw HeaderNotFoundValueException(e._1)
      }
      (e._1 -> v)
    })

    val nrows = mHeaderVal.get("nrows").get.toInt
    val ncols = mHeaderVal.get("ncols").get.toInt
    val xllcorner = mHeaderVal.get("xllcorner").get.replace(",", ".").toDouble
    val yllcorner = mHeaderVal.get("yllcorner").get.replace(",", ".").toDouble
    val cellsize = mHeaderVal.get("cellsize").get.replace(",", ".").toDouble

    val ulLatLon = CoordinateConverter.utm64z34NToLatLon(
      xllcorner,
      yllcorner+nrows*cellsize
    )
    val lrLatLon = CoordinateConverter.utm64z34NToLatLon(
      xllcorner+ncols*cellsize,
      yllcorner
    )
    val cells: List[List[BathymetryCell]] = Source.fromFile(filename).getLines().dropWhile(l => l.matches(reHeader))
      .toList
      .map(line => line.replaceAll(",", ".").split("\\s+").toList.map(v => {
        BathymetryCell(if (v == "-9999") None else (Some(v.toDouble)))
      }
      ))
    BathymetryGrid(
      nrows,
      ncols,
      LatLonBBox(ulLatLon, lrLatLon),
      cells
    )
  }
}