package ch.octo.terrasubmersa.tiles.io.reader

import java.io.File

import ch.octo.terrasubmersa.application.GenerateImageTiles.refCoord
import ch.octo.terrasubmersa.coordinates.{LatLonBBox, LatLonCoords, Meter}
import uk.me.jstott.jcoord.UTMRef

import scala.io.Source

object SensysReader {

  case class CannotParserDescriptionFileException(msg: String) extends Exception(msg)

  val reField = """\\*(\w+)\.*:\s*(.*)""".r
  val reMeter = """([0-9\.]+)m""".r

  /**
    * extract bouding box from the tif.txt file
    * from Xol and Yol fields, plus dX dY
    *
    * working in Kilada Bay, we fix for the moment the UTM grid to be 34N
    *
    * @param txtFile
    * @return
    */
  def readBoundingBox(txtFile: File): LatLonBBox = {

    val valFields: Map[String, String] = Source.fromFile(txtFile, "ISO-8859-1").getLines()
      .flatMap(l => reField.findAllMatchIn(l))
      .map(m => (m.group(1) -> m.group(2)))
      .toMap

    val reMeter(xol) = valFields("Xol")
    val reMeter(yol) = valFields("Yol")
    val reMeter(dX) = valFields("dX")
    val reMeter(dY) = valFields("dY")

    val utm = new UTMRef(xol.toDouble, yol.toDouble, 'N', 34)
    val ll = utm.toLatLng
    val refCoord = LatLonCoords(ll.getLat, ll.getLng)
    LatLonBBox(refCoord, refCoord.plusHoriz(Meter(dX.toDouble)).plusVert(Meter(-dY.toDouble)))
  }

  /**
    * Read the first line and returns it as a decscription
    * @param txtFile
    * @return
    */
  def readDescription(txtFile: File): String = {
    Source.fromFile(txtFile, "ISO-8859-1").getLines().take(1).mkString("")
  }
}
