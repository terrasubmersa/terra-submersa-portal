package ch.octo.terrasubmersa.tiles.image

import java.io.File

import ch.octo.terrasubmersa.coordinates.{HasLatLonBBox, LatLonBBox}
import ch.octo.terrasubmersa.tiles.slippy.SlippyTile
import com.sksamuel.scrimage.Image
import com.sksamuel.scrimage.nio.PngWriter
import org.apache.commons.io.FileUtils

/**
  * Created by alex on 23.08.17.
  */
case class ImageBBox(image: Image, bbox: LatLonBBox) extends HasLatLonBBox {
  //case class ImageBBox(imageWidth: Int, imageHeight: Int,  bbox: LatLongBBox) extends HasLatLonBBox {
  override val toBBox: LatLonBBox = bbox
  val imageWidth: Int = image.width
  val imageHeight: Int = image.height

  /**
    * save the image to a PNG image tile, by cropping, padding and resizing
    *
    * @param tile the target tile
    * @param dir  the root directory
    */
  def saveTileImage(tile: SlippyTile, dir: String): Unit = {
    saveTileImage(tile, new File(dir))
  }

  /**
    * save the image to a PNG image tile, by cropping, padding and resizing
    *
    * @param tile the target tile
    * @param dir  the root directory
    */
  def saveTileImage(tile: SlippyTile, dir: File): Unit = {
    val transfoLat = ImageTransfo.build(
      tile.toBBox.lowerRight.lat,
      tile.toBBox.upperLeft.lat,
      toBBox.lowerRight.lat,
      toBBox.upperLeft.lat,
      ImageBBox.TILE_SIZE,
      imageHeight
    )
    val transfoLon = ImageTransfo.build(
      tile.toBBox.upperLeft.lon,
      tile.toBBox.lowerRight.lon,
      toBBox.upperLeft.lon,
      toBBox.lowerRight.lon,
      ImageBBox.TILE_SIZE,
      imageWidth
    )

    FileUtils.forceMkdir(tile.toDir(dir))


    val scaleToWidth = ((imageWidth - transfoLon.crop0 - transfoLon.crop1) * transfoLon.scale).toInt
    val scaleToHeight = ((imageHeight - transfoLat.crop0 - transfoLat.crop1) * transfoLat.scale).toInt
    val trimLeft = transfoLon.crop0
    val trimTop = transfoLat.crop1
    val trimRight = transfoLon.crop1
    val trimBottom = transfoLat.crop0
    val padLeft = transfoLon.pad0
    val padTop = transfoLat.pad1
    val padRight = transfoLon.pad1
    val padBottom = transfoLat.pad0

    val actualScaleToWidth = math.max(scaleToWidth,3)
    val actualScaleToHeight = math.max(scaleToHeight,3)

    image.trim(trimLeft, trimTop, trimRight, trimBottom)
      .scaleTo(actualScaleToWidth, actualScaleToHeight)
      .padWith(padLeft, padTop, padRight, padBottom)
      .output(tile.toFile(dir).getAbsolutePath)(PngWriter())
  }


}

object ImageBBox {
  val TILE_SIZE = 256
}
