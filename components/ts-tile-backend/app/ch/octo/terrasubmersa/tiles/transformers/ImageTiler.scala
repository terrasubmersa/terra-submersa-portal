package ch.octo.terrasubmersa.tiles.transformers

import java.io.File

import ch.octo.terrasubmersa.coordinates.LatLonBBox
import ch.octo.terrasubmersa.slippy.ZoomRange
import ch.octo.terrasubmersa.tiles.image.ImageBBox
import ch.octo.terrasubmersa.tiles.slippy.SlippyTileNode
import com.sksamuel.scrimage.Image

import scala.concurrent.{ExecutionContext, Future}

object ImageTiler {
  /**
    * convert an image into a hierachy of file tiles
    * @param imageFile the original image file
    * @param bbox the latitude/longitude bounding box
    * @param outDir where to place the tiles hierarchy
    * @param zoom The range with max = the maximum zoom level; min: if set, the minimum (coarser grain) zoom level. If not present, it is taken from the bbox
    */
  def tile(imageFile: File, bbox: LatLonBBox, outDir: File, zoom:ZoomRange)(implicit ec:ExecutionContext): Future[Unit] = {
    val image = Image.fromFile(imageFile)
    val imageBBox = ImageBBox(image, bbox)

    val initTile = bbox.minimumOverlayTile(zoom.min).get

    Future {
      SlippyTileNode.walkAndDo(initTile,
        zoom.max,
        (tile) => bbox.isIntersect(tile.toBBox),
        (tile) => imageBBox.saveTileImage(tile, outDir)
      )
    }
  }
}
