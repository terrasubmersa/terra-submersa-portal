package ch.octo.terrasubmersa.coordinates

import ch.octo.terrasubmersa.slippy.UndefinedSlippyTileException
import ch.octo.terrasubmersa.tiles.slippy.SlippyTile

import scala.util.{Failure, Success, Try}

/**
  * Created by alex on 22.08.17.
  */
case class LatLonBBox(upperLeft: LatLonCoords, lowerRight: LatLonCoords) {
  assert(upperLeft.lat >= lowerRight.lat)
  assert(upperLeft.lon <= lowerRight.lon)

  def isIntersect(other: LatLonBBox): Boolean = {
    (other.lowerRight.lon >= upperLeft.lon) &&
      (other.upperLeft.lon <= lowerRight.lon) &&
      (other.upperLeft.lat >= lowerRight.lat) &&
      (other.lowerRight.lat <= upperLeft.lat)
  }

  /**
    * is the other LatLongBBox totally included within this one?
    *
    * @param other an other LatLongBBox
    * @return
    */
  def isIncluding(other: LatLonBBox): Boolean = {
    (other.lowerRight.lon <= lowerRight.lon) &&
      (other.upperLeft.lon >= upperLeft.lon) &&
      (other.upperLeft.lat <= upperLeft.lat) &&
      (other.lowerRight.lat >= lowerRight.lat)
  }

  /**
    * get the minimum tile overlaying the LatLongBBox.
    * Not to find an overlaying box, I cannot find a situation
    *
    * @param minZoom if defined, force a lower bound on the zoom layer. If not, the search stos when the small tile covering the area is found.
    * @return the Success(SlippyTile) or a UndefinedSlippyTileException
    */
  def minimumOverlayTile(minZoom: Option[Int] = None): Try[SlippyTile] = {
    val ost = (SlippyTile.MAX_LEVEL to SlippyTile.MIN_LEVEL by -1)
      .map(z => upperLeft.toTile(z))
      .dropWhile(st => minZoom match {
        case Some(l) =>
          st.z > l
        case None => !st.toBBox.isIncluding(this)
      })
      .headOption
    ost match {
      case None => Failure(UndefinedSlippyTileException("cannot find a slippy tile around $this"))
      case Some(st) => Success(st)
    }
  }
}
