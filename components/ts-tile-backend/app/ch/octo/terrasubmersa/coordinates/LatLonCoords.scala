package ch.octo.terrasubmersa.coordinates

import ch.octo.terrasubmersa.tiles.slippy.SlippyTile
import ch.octo.terrasubmersa.coordinates.CoordinateSystem._

import scala.math._

/**
  * Created by alex on 21.08.17.
  */
case class LatLonCoords(lat: Double, lon: Double) {
  val EARTH_RADIUS = 6371000
  val EARTH_CIRCUMFERENCE = 2 * math.Pi * EARTH_RADIUS

  def toTileFraction(z: Int): XYCoords = {
    XYCoords(
      ((lon + 180.0) / 360.0 * (1 << z)),
      ((1 - log(tan(toRadians(lat)) + 1 / cos(toRadians(lat))) / Pi) / 2.0 * (1 << z))
    )
  }

  def toTile(z: Int): SlippyTile = {
    val xy = toTileFraction(z)
    SlippyTile(math.floor(xy.x).toInt, math.floor(xy.y).toInt, z)
  }

  def plusHoriz(p: Meter): LatLonCoords = {
    val circLat = EARTH_CIRCUMFERENCE * math.cos(lat / 180 * math.Pi)
    LatLonCoords(lat, lon + 360 * p.value / circLat)
  }

  def plusVert(p: Meter): LatLonCoords = {
    LatLonCoords(lat + 360 * p.value / EARTH_CIRCUMFERENCE, lon)
  }
}

object LatLonCoords {
  def degFromString(str: String): Double = {
    val re1 = """(\d+).+?(\d+)'\s*([\d\.]+)\"""".r
    str.trim match {
      case re1(d, m, s) => d.toDouble + m.toDouble / 60 + s.toDouble / 3600
      case a => a.toDouble
    }
  }

  def fromString(strLat: String, strLon: String, coordSystem: CoordinateSystem): LatLonCoords = {
    coordSystem match {
      case WGS84 => LatLonCoords(degFromString(strLat), degFromString(strLon))
      case UTM34N => CoordinateConverter.utm64z34NToLatLon(strLon.toDouble, strLat.toDouble)
    }
  }
}