package ch.octo.terrasubmersa

/**
  * Created by alex on 10.09.17.
  */
package object coordinates {
  case class Meter(value:Double) extends AnyVal

  object CoordinateSystem extends Enumeration{
    type  CoordinateSystem = Value
      val WGS84, UTM34N = Value
  }
}
