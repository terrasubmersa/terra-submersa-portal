package ch.octo.terrasubmersa.application

import java.io.File

import ch.octo.terrasubmersa.slippy.ZoomRange
import ch.octo.terrasubmersa.tiles.io.reader.SensysReader
import ch.octo.terrasubmersa.tiles.transformers.ImageTiler

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by alex on 22.08.17.
  */
object GenerateSensysImageTiles extends App {
  val inImageFile = new File("/Users/alex/dev/terra-submersa/data/kilada-bay-2016/SENSYS/Geo_Tiff/july11-12.png")
  val inTxtFile = new File("/Users/alex/dev/terra-submersa/data/kilada-bay-2016/SENSYS/Geo_Tiff/july11-12.tif.txt")

  def outDir = new File("/tmp/ts/images/xxx")

  def boundaries = SensysReader.readBoundingBox(inTxtFile)

  def zoom = ZoomRange(Some(15), 19)

  ImageTiler.tile(inImageFile, boundaries, outDir, zoom)

}
