package ch.octo.terrasubmersa.application

import java.io.File

import ch.octo.terrasubmersa.coordinates.{LatLonBBox, LatLonCoords, Meter}
import ch.octo.terrasubmersa.slippy.ZoomRange
import ch.octo.terrasubmersa.tiles.image.ImageBBox
import ch.octo.terrasubmersa.tiles.slippy.SlippyTileNode
import ch.octo.terrasubmersa.tiles.transformers.ImageTiler
import com.sksamuel.scrimage.Image

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by alex on 22.08.17.
  */
object GenerateImageTiles extends App {
  //  def boundaries = LatLonBBox(LatLonCoords(37.46029356319137, 23.09417842005971), LatLonCoords(37.39003123767094, 23.14084352618228))
  //  def imgPath = "data/porto_heli11.kmz_unzipped/Composite.png"
  //  def outDir = "/tmp/generate-tiles"

  //  def boundaries =   LatLonBBox(LatLonCoords(37.42252593456306,23.126220703125),LatLonCoords(37.41816326969145,23.1317138671875))
  //  def imgPath = "../terra-submersa-frontend/src/dev-data/osm-tiles/heli11/orig/16-36978-25412.png"
  //  def outDir = "../terra-submersa-frontend/src/dev-data/osm-tiles/heli11"

  //val refCoord = LatLonCoord  s(37.42252593456306, 23.126220703125)
  val refCoord = LatLonCoords(37.42677698171437, 23.13419459944256)

  def boundaries = LatLonBBox(refCoord.plusVert(Meter(282.07)), refCoord.plusHoriz(Meter(142.1)))
  def imgPath = "/Users/alex/dev/terra-submersa/data/kilada-bay-2016/SENSYS/Geo_Tiff/JULY_ALL_LAND.png"
  def outDir = "/tmp/ts/images/JULY_ALL_LAND"

  def zoom = ZoomRange(Some(12),19)
  ImageTiler.tile(new File(imgPath), boundaries, new File(outDir), zoom)

  //  imageBBox.toTileImage(SlippyTile(9243, 6350, 14), "/tmp/generate-tiles")
  //  imageBBox.toTileImage(SlippyTile(9243, 6351, 14), "/tmp/generate-tiles")
  //  imageBBox.toTileImage(SlippyTile(9243, 6352, 14), "/tmp/generate-tiles")

}
