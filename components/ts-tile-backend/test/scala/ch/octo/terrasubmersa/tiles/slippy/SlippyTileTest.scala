package ch.octo.terrasubmersa.tiles.slippy

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by alex on 18.08.17.
  */
class SlippyTileTest extends FlatSpec with Matchers {
  "tile (156, 123, 9).toBBox" should "give the correct BBox" in {
    def tile = SlippyTile(156, 123, 9)

    val bbox = tile.toBBox
    bbox.upperLeft.lat should equal(67.87 +- 0.01)
    bbox.upperLeft.lon should equal(-70.31 +- 0.01)

    bbox.lowerRight.lat should equal(67.6 +- 0.01)
    bbox.lowerRight.lon should equal(-69.61 +- 0.01)
  }

  "tile (4622, 3176, 13).toBBox" should "give the correct BBox" in {
    def tile = SlippyTile(4622, 3176, 13)

    val bbox = tile.toBBox
    bbox.upperLeft.lat should equal(37.44 +- 0.01)
    bbox.upperLeft.lon should equal(23.12 +- 0.01)

    bbox.lowerRight.lat should equal(37.41 +- 0.01)
    bbox.lowerRight.lon should equal(23.16 +- 0.01)
  }

  "p25412af" should "give the correct BBox" in {
    def tile = SlippyTile(36978, 25412, 16)

    val bbox = tile.toBBox
  }
  "subTiles" should "(0,0,0)" in {
    SlippyTile(0, 0, 0).subTiles should equal(Set(
      SlippyTile(0, 0, 1),
      SlippyTile(0, 1, 1),
      SlippyTile(1, 0, 1),
      SlippyTile(1, 1, 1)
    ))
  }
  "subTiles" should "(119, 191, 9)" in {
    SlippyTile(119, 191, 9).subTiles should equal(Set(
      SlippyTile(238, 382, 10),
      SlippyTile(239, 382, 10),
      SlippyTile(238, 383, 10),
      SlippyTile(239, 383, 10)
    ))
  }
}
