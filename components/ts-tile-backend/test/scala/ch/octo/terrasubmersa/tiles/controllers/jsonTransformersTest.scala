package ch.octo.terrasubmersa.tiles.controllers

import ch.octo.terrasubmersa.annotations.{Label, TextAnnotation, TextAnnotationId}
import ch.octo.terrasubmersa.coordinates.{LatLonBBox, LatLonCoords}
import ch.octo.terrasubmersa.tiles.controllers.JsonTransformers._
import ch.octo.terrasubmersa.tiles.models.{TileSystem, TileSystemBoundaries, TileSystemId}
import org.scalatest.{FlatSpec, Matchers}
import play.api.libs.json._


class jsonTransformersTest extends FlatSpec with Matchers {

  behavior of "jsonTransformersTest"
  private val objUpperLeft = LatLonCoords(12.34, 56.78)
  private val objLowerRight = LatLonCoords(-12.34, 156.78)
  private val strUpperLeft = """{"lat":12.34, "lon":56.78 } """
  private val strLowerRight = """{"lat":-12.34, "lon":156.78 } """
  private val jsUpperLeft = JsObject(Seq("lat" -> JsNumber(12.34), "lon" -> JsNumber(56.78)))
  private val jsLowerRight = JsObject(Seq("lat" -> JsNumber(-12.34), "lon" -> JsNumber(156.78)))

  private val objLatLonBBox = LatLonBBox(objUpperLeft, LatLonCoords(-12.34, 156.78))
  private val strLatLonBBox =
    s"""{"upperLeft" : $strUpperLeft,"lowerRight" : $strLowerRight}"""
  private val jsLatLonBBox = JsObject(Seq("upperLeft" -> jsUpperLeft, "lowerRight" -> jsLowerRight))


  private val objTileSystemBoundaries = TileSystemBoundaries(12, 19, objLatLonBBox)
  private val strTileSystemBoundaries =
    s"""{"minLevel":12,"maxLevel":19,"bbox":$strLatLonBBox }"""
  val jsTileSystemBoundaries = JsObject(Seq(
    "minLevel" -> JsNumber(12),
    "maxLevel" -> JsNumber(19),
    "bbox" -> jsLatLonBBox
  ))

  private val strTileSystemId = """"abc""""
  private val objTileSystemId = TileSystemId("abc")
  private val jsTileSystemId = JsString("abc")

  private val objTileSystem = TileSystem(
    objTileSystemId,
    objTileSystemBoundaries,
    "http://aaa/{z}/{x}/{y}",
    "my title",
    "WTF",
    "paf"
  )

  private val strTileSystem =
    s"""{
       |"id":"abc",
       |"boundaries":$strTileSystemBoundaries,
       |"urlMask":"http://aaa/{z}/{x}/{y}",
       |"title": "my title",
       |"description": "WTF",
       |"copyright": "paf"
       |}""".stripMargin
  private val jsTileSystem = JsObject(Seq(
    "id" -> jsTileSystemId,
    "boundaries" -> jsTileSystemBoundaries,
    "urlMask" -> JsString("http://aaa/{z}/{x}/{y}"),
    "title" -> JsString("my title"),
    "description" -> JsString("WTF"),
    "copyright" -> JsString("paf"),

  ))

  private val strLabel = """"one label""""
  private val objLabel = Label("one label")
  private val jsLabel = JsString("one label")

  private val strTextAnnotation =
    s"""{
       |"id":"ta.id",
       |"text":"whatever",
       |"position":$strUpperLeft,
       |"tileSystemId":$strTileSystemId,
       |"labels":["one","two"]
       |}""".stripMargin
  private val objTextAnnotation = TextAnnotation(
    Some(TextAnnotationId("ta.id")),
    "whatever",
    objUpperLeft,
    objTileSystemId,
    Set(Label("one"), Label("two"))
  )
  private val jsTextAnnotation = JsObject(Seq(
    "id" -> JsString("ta.id"),
    "text" -> JsString("whatever"),
    "position" -> jsUpperLeft,
    "tileSystemId" -> jsTileSystemId,
    "labels" -> JsArray(Seq(JsString("one"), JsString("two")))
  ))

  private val strTextAnnotationNoneId =
    s"""{
       |"text":"whatever",
       |"position":$strUpperLeft,
       |"tileSystemId":$strTileSystemId,
       |"labels":["one","two"]
       |}""".stripMargin
  private val objTextAnnotationNoneId = TextAnnotation(
    None,
    "whatever",
    objUpperLeft,
    objTileSystemId,
    Set(Label("one"), Label("two"))
  )
  private val jsTextAnnotationNoneId = JsObject(Seq(
    "id"-> JsNull,
    "text" -> JsString("whatever"),
    "position" -> jsUpperLeft,
    "tileSystemId" -> jsTileSystemId,
    "labels" -> JsArray(Seq(JsString("one"), JsString("two")))
  ))


  it should "LatLonCoordsReads" in {
    val jsonValue: JsValue = Json.parse(strUpperLeft)

    val llRes = Json.fromJson[LatLonCoords](jsonValue)

    llRes should be(JsSuccess(objUpperLeft))

  }
  it should "LatLonCoordsWrites" in {
    val js = Json.toJson(objUpperLeft)

    js should equal(jsUpperLeft)
  }


  it should "LatLonBBoxReads" in {
    val jsonValue: JsValue = Json.parse(strLatLonBBox)

    val res = Json.fromJson[LatLonBBox](jsonValue)

    res should be(JsSuccess(objLatLonBBox))
  }
  it should "LatLonBBoxWrites" in {
    val js = Json.toJson(objLatLonBBox)

    js should equal(jsLatLonBBox)
  }

  it should "TileSystemBoundariesReads" in {
    val jsonValue: JsValue = Json.parse(strTileSystemBoundaries)

    val res = Json.fromJson[TileSystemBoundaries](jsonValue)

    res should be(JsSuccess(objTileSystemBoundaries))

  }
  it should "TileSystemBoundariesWrites" in {
    val js = Json.toJson(objTileSystemBoundaries)

    js should equal(jsTileSystemBoundaries)
  }

  it should "TileSystemIdRead" in {
    val jsonValue: JsValue = Json.parse(strLabel)

    val res = Json.fromJson[Label](jsonValue)

    res should be(JsSuccess(objLabel))
  }
  it should "TileSystemIdWrites" in {
    val js = Json.toJson(objLabel)

    js should equal(jsLabel)

  }


  it should "TileSystemReads" in {
    val jsonValue: JsValue = Json.parse(strTileSystem)

    val res = Json.fromJson[TileSystem](jsonValue)

    res should be(JsSuccess(objTileSystem))
  }
  it should "TileSystemWrites" in {
    val js = Json.toJson(objTileSystem)

    js should equal(jsTileSystem)

  }
  it should "LabelRead" in {
    val jsonValue: JsValue = Json.parse(strLabel)

    val res = Json.fromJson[Label](jsonValue)

    res should be(JsSuccess(objLabel))
  }
  it should "LabelWrites" in {
    val js = Json.toJson(objLabel)

    js should equal(jsLabel)

  }
  it should "TextAnnotationRead" in {
    val jsonValue: JsValue = Json.parse(strTextAnnotation)
    val res = Json.fromJson[TextAnnotation](jsonValue)

    res should be(JsSuccess(objTextAnnotation))
  }
  it should "TextAnnotationWrites" in {
    val js = Json.toJson(objTextAnnotation)

    js should equal(jsTextAnnotation)

  }

  it should "TextAnnotationRead none id" in {
    val jsonValue: JsValue = Json.parse(strTextAnnotationNoneId)
    val res = Json.fromJson[TextAnnotation](jsonValue)

    res should be(JsSuccess(objTextAnnotationNoneId))
  }
  it should "TextAnnotationWrites  none id" in {
    val js = Json.toJson(objTextAnnotationNoneId)

    js should equal(jsTextAnnotationNoneId)

  }
}
