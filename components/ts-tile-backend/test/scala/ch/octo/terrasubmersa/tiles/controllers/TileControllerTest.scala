package ch.octo.terrasubmersa.tiles.controllers

import org.scalatest.{FlatSpec, Matchers}

class TileControllerTest extends FlatSpec with Matchers{

  behavior of "TileControllerTest"

  it should "filename2id" in {
    val filename = "paf.le.chien.png"

    val id =  TileController.filename2id(filename)

    id should be("paf.le.chien")
  }

}
