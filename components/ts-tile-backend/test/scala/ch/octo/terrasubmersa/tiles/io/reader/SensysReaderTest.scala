package ch.octo.terrasubmersa.tiles.io.reader

import java.io.File

import ch.octo.terrasubmersa.coordinates.{LatLonBBox, LatLonCoords}
import org.scalatest.{FlatSpec, Matchers}

class SensysReaderTest extends FlatSpec with Matchers{
  val txtFile = new File("test/resources/sensys-test.tif.txt")

  "test file" should "exists" in {
    txtFile.exists() should be(true)
  }

  "readBoundingBox" should "extract coordinates" in {
    val bbox = SensysReader.readBoundingBox(txtFile)
    val upperLeft = bbox.upperLeft
    val lowerRight = bbox.lowerRight

    upperLeft.lat should be(37.42829575636808 +- 0.000001)
    upperLeft.lon should be(23.133338429223993 +- 0.000001)
    lowerRight.lat should be(37.42650169969643 +- 0.000001)
    lowerRight.lon should be(23.1338479335279 +- 0.000001)
  }

  "readDescription" should "extract description" in {
    val descr = SensysReader.readDescription(txtFile)

    descr should equal("Autogenerierte Datei erzeugt von SENSYS MAGNETO(R)2.x")
  }
}
