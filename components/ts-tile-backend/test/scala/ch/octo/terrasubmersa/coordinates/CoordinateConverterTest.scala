package ch.octo.terrasubmersa.coordinates

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by alex on 09.09.17.
  */
class CoordinateConverterTest extends FlatSpec with Matchers {
  "(685864.77486308, 4141222.5066288) UTM64 34N" should "converte to lat/lon(23.09982460, 37.39915007)" in{
    val ll = CoordinateConverter.utm64z34NToLatLon(685864, 4141222)
    ll.lat should equal (37.39915007 +- 0.00001)
    ll.lon should equal (23.09982460 +- 0.00001)
  }
  
}
