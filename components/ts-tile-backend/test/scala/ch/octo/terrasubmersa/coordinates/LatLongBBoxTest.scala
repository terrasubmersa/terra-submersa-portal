package ch.octo.terrasubmersa.coordinates

import ch.octo.terrasubmersa.tiles.slippy.SlippyTile
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FlatSpec, Matchers}

import scala.util.Success

/**
  * Created by alex on 22.08.17.
  */
class LatLongBBoxTest extends FlatSpec with Matchers with TableDrivenPropertyChecks {
  an[AssertionError] should be thrownBy {
    LatLonBBox(LatLonCoords(45, 10), LatLonCoords(43, 5))
  }

  val bb0 = LatLonBBox(LatLonCoords(45, -20), LatLonCoords(35, 10))
  "isIntersect" should "be true when the same" in {
    bb0.isIntersect(bb0) should be(true)
  }

  val bb0IntersectionSpecs = Table(
    ("bbUlLat", "bbUlLon", "bbLRLat", "bbLRLon", "isBB0Intersect", "comment"),
    (50, -30, 25, 30, true, "be true when inner"),
    (48, -10, 38, 15, true, "be true when outer"),
    (45, -30, 35, -25, false, "be false when on left"),
    (45, 20, 5, 25, false, "be false when on right"),
    (70, -20, 50, 35, false, "be false when on top"),
    (20, -20, 10, 35, false, "be false when on bottom"),
    (20, -20, 10, 35, false, "be false when on bottom"),
    (43, -30, 38, -10, true, "be true when intersect left"),
    (43, 0, 38, 20, true, "be true when intersect right"),
    (38, -15, 28, 5, true, "be true when intersect bottom"),
    (58, -15, 8, 5, true, "be true when intersect vertical bar"),
    (43, -25, 38, 25, true, "be true when intersect horizontal bar")
  )

  forAll(bb0IntersectionSpecs) {
    (bbUlLat: Int, bbUlLon: Int, bbLRLat: Int, bbLRLon: Int, isBB0Intersect: Boolean, comment: String) =>
      val isIntersect = bb0.isIntersect(LatLonBBox(LatLonCoords(bbUlLat, bbUlLon), LatLonCoords(bbLRLat, bbLRLon)))
      withClue(comment) {
        isIntersect should be(isBB0Intersect)
      }
  }



  "isIncluding" should "be true when myself" in {
    bb0.isIncluding(bb0) should be(true)
  }
  "isIncluding" should "be false when inner" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(50, -30), LatLonCoords(25, 30))) should be(false)
  }
  "isIncluding" should "be true when outer" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(43, -10), LatLonCoords(38, 8))) should be(true)
  }
  "isIncluding" should "be false when on left" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(45, -30), LatLonCoords(35, -25))) should be(false)
  }
  "isIncluding" should "be false when on right" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(45, 20), LatLonCoords(35, 25))) should be(false)
  }
  "isIncluding" should "be false when on top" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(70, -20), LatLonCoords(50, 35))) should be(false)
  }
  "isIncluding" should "be false when on bottom" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(20, -20), LatLonCoords(10, 35))) should be(false)
  }
  "isIncluding" should "be false when intersect left" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(43, -30), LatLonCoords(38, -10))) should be(false)
  }
  "isIncluding" should "be false when intersect right" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(43, 0), LatLonCoords(38, 20))) should be(false)
  }
  "isIncluding" should "be false when intersect top" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(48, -15), LatLonCoords(38, 5))) should be(false)
  }
  "isIncluding" should "be false when intersect bottom" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(38, -15), LatLonCoords(28, 5))) should be(false)
  }
  "isIncluding" should "be false when intersect vertical bar" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(58, -15), LatLonCoords(8, 5))) should be(false)
  }
  "isIncluding" should "be false when intersect horizontal bar" in {
    bb0.isIncluding(LatLonBBox(LatLonCoords(43, -25), LatLonCoords(38, 25))) should be(false)
  }

  "minimumOverlayTile" should "(-96.29, 41.49)x(-95.65, 41.02) -> (119, 191, 9)" in {
    val bb = LatLonBBox(LatLonCoords(41.49, -96.29), LatLonCoords(41.02, -95.65))
    bb.minimumOverlayTile() should equal(Success(SlippyTile(119, 191, 9)))
  }
}
