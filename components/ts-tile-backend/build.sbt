name := scala.sys.env.getOrElse("PROJECT_NAME", "ts-tile-backend")

version := scala.sys.env.getOrElse("VERSION", "0.0.0-SNAPSHOT")

scalaVersion := "2.12.3"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq(
  guice,
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "com.sksamuel.scrimage" %% "scrimage-core" % "2.1.8",
  "com.sksamuel.scrimage" %% "scrimage-io-extra" % "2.1.8",
  "commons-io" % "commons-io" % "2.5",
  "org.apache.commons" % "commons-math3" % "3.0",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
  "org.reactivemongo" % "reactivemongo_2.12" % "0.12.7",
  "org.zeroturnaround" % "zt-zip" % "1.12"
)

import com.typesafe.sbt.packager.docker._

dockerCommands ++= Seq(
    Cmd("USER", "root"),
    Cmd("RUN", "mkdir", "-p", "/data/tiles/osm-tiles"),
    Cmd("RUN", "chown", "-R", (daemonUser in Docker).value, "/data/tiles"),
    Cmd("USER", (daemonUser in Docker).value),
    Cmd("EXPOSE", "9000")
)
