# Terra Submersa Frontend

Based on AngularJS, this component is to display a map with superimposed measure artefact


## The dev side of it
### Dev data

Only a small area is saved within the project. It corresponds to a Kilada Bay OSM tile, with coordinates
z=16 x=36978 y=25412


## Local build with Docker
### Build
    docker run -it -v "$PWD":/workspace node:8 sh -c "cd /workspace && npm install && npm install && ./node_modules/@angular/cli/bin/ng build --prod"
    
### Unit test

    docker run -it -v "$PWD":/workspace alexmass/node-chrome-headless:8 sh -c "cd /workspace && npm install && ./node_modules/@angular/cli/bin/ng test --single-run=true --browsers ChromeHeadless --watch=false"

### Sonar 
    docker run --rm -v "$PWD":/data 5gsystems/node-sonar-scanner:1.0.0 sonar-scanner -Dsonar.login=$SONAR_LOGIN

### With build local installation
#### Development server

    nvm use 8
    npm install -g @angular/cli
    npm install

Run `npm start` to start the dev server. Navigate to `http://localhost:4200/`. 


#### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Deploy
    export VERSION=$(cat ../../PROJECT_VERSION.txt)
    export PROJECT_NAME=$(basename $(pwd))

    ng build --prod
    rm -r dist/assets/dev-data
    docker build -t $PROJECT_NAME .

    docker tag $PROJECT_NAME $PROJECT_NAME:$VERSION
    docker tag $PROJECT_NAME:$VERSION registry.gitlab.com/terrasubmersa/terra-submersa-portal/$PROJECT_NAME:$VERSION
    docker tag registry.gitlab.com/terrasubmersa/terra-submersa-portal/$PROJECT_NAME:$VERSION registry.gitlab.com/terrasubmersa/terra-submersa-portal/$PROJECT_NAME:latest


### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

