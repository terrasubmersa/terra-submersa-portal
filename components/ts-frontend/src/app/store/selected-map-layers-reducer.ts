import * as SelectedMapLayersActions from './selected-map-layers-actions';
import {SelectedMapLayers} from '../models/selected-map-layers';

export type Action = SelectedMapLayersActions.All;

/**
 *
 * @param state
 * @param action
 * @return {any}
 */
export function selectedMapLayersReducer(state: SelectedMapLayers = new SelectedMapLayers(), action: Action) {
  switch (action.type) {
    case SelectedMapLayersActions.SELECTED_MAP_LAYERS_SET_AVAILABLE:
      return state.setAvailable(action.availableMapLayers);
    case SelectedMapLayersActions.SELECTED_MAP_LAYERS_SELECT:
      if(action.isSelected){
        return state.select(action.id);
      }else{
        return state.unselect(action.id);
      }
    case SelectedMapLayersActions.SELECTED_MAP_LAYERS_SELECT_ALL:
      return state.selectAll();
    default:
;      return state;
  }
}
