import {Action} from '@ngrx/store';
import {AvailableMapLayers} from '../models/available-map-layers';

export const SELECTED_MAP_LAYERS_SELECT = '[SelectedMapLayers] select';
export const SELECTED_MAP_LAYERS_SELECT_ALL = '[SelectedMapLayers] selectAll';
export const SELECTED_MAP_LAYERS_SET_AVAILABLE = '[SelectedMapLayers] setAvailable';

export class SetAvailableLayers implements Action {
  readonly type = SELECTED_MAP_LAYERS_SET_AVAILABLE;

  constructor(public availableMapLayers: AvailableMapLayers) {
  }
}

export class SelectState implements Action {
  readonly type = SELECTED_MAP_LAYERS_SELECT;

  constructor(public id: string, public isSelected: boolean) {
  }
}

export class SelectAll implements Action {
  readonly type = SELECTED_MAP_LAYERS_SELECT_ALL;

}

export type All
  = SelectState | SelectAll | SetAvailableLayers;
