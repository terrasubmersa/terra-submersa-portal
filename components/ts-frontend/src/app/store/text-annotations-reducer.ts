import * as TextAnnotationActions from './text-annotations-actions';
import {TextAnnotationSet} from '../models/text-annotation-set';

export type Action = TextAnnotationActions.All;

/**
 *
 * @param state
 * @param action
 * @return {any}
 */
export function textAnnotationsReducer(state: TextAnnotationSet = new TextAnnotationSet(), action: Action) {
  switch (action.type) {
    case TextAnnotationActions.TEXT_ANNOTATION_SET_AVAILABLE:
      return state.setAvailable(action.annotations);
    case TextAnnotationActions.TEXT_ANNOTATION_VISIBILITY:
      return state.setVisibility(action.visibility);
    case TextAnnotationActions.TEXT_ANNOTATION_UPDATE:
      return state.update(action.textAnnotation);
    case TextAnnotationActions.TEXT_ANNOTATION_REMOVE:
      return state.remove(action.textAnnotation);
    default:
      return state;
  }
}
