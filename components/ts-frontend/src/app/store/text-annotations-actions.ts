import {Action} from '@ngrx/store';
import {TextAnnotation} from '../models/text-annotation';


export const TEXT_ANNOTATION_SET_AVAILABLE = '[TextAnnotation] setAvailable';
export const TEXT_ANNOTATION_VISIBILITY = '[TextAnnotation] setVisibility';
export const TEXT_ANNOTATION_UPDATE = '[TextAnnotation] update';
export const TEXT_ANNOTATION_REMOVE = '[TextAnnotation] remove';

export class SetAvailableTextAnnotations implements Action {
  readonly type = TEXT_ANNOTATION_SET_AVAILABLE;

  constructor(public annotations: TextAnnotation[]) {
  }
}

export class SetTextAnnotationsVisibility implements Action {
  readonly type = TEXT_ANNOTATION_VISIBILITY;

  constructor(public visibility: boolean) {
  }
}

export class UpdateTextAnnotation implements Action {
  readonly type = TEXT_ANNOTATION_UPDATE;

  constructor(public textAnnotation: TextAnnotation) {
  }
}

export class RemoveTextAnnotation implements Action {
  readonly type = TEXT_ANNOTATION_REMOVE;

  constructor(public textAnnotation: TextAnnotation) {
  }
}


export type All
  = SetAvailableTextAnnotations |
  SetTextAnnotationsVisibility |
  UpdateTextAnnotation |
  RemoveTextAnnotation;
