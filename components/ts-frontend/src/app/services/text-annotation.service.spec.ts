import { TestBed, inject } from '@angular/core/testing';

import { TextAnnotationService } from './text-annotation.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';

describe('TextAnnotationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TextAnnotationService],
      imports: [
        HttpClientTestingModule,
      ]
    });
  });

  it('should be created', inject([HttpClient, TextAnnotationService], (http: HttpClient, service: TextAnnotationService) => {
    expect(service).toBeTruthy();
  }));
});
