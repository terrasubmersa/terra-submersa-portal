import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TextAnnotation} from '../models/text-annotation';
import {LatLonCoords} from '../models/lat-lon-coord';
import {environment} from '../../environments/environment';
import * as _ from 'lodash';

@Injectable()
export class TextAnnotationService {
  private readonly urlAPI: string;

  constructor(private http: HttpClient) {
    this.urlAPI = environment.urlTileBackend + '/api/text-annotation';
  }

  getList(): Promise<TextAnnotation[]> {
    const self = this;
    return this.http.get(self.urlAPI + 's')
      .toPromise()
      .then(res => {
        return _.map(res, (resObj: any) => {
          return new TextAnnotation(
            resObj.id,
            resObj.text,
            resObj.position as LatLonCoords,
            resObj.tileSystemId,
            resObj.labels as string[]
          )
        });
      });
  }


  update(ta: TextAnnotation): Promise<any> {
    const self = this;
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    const url = self.urlAPI + (ta.id ? ('/' + ta.id) : '');
    return self.http.post(
      url,
      ta,
      {headers: headers}
    ).toPromise()
  }

  remove(ta: TextAnnotation): Promise<any> {
    const self = this;
    const url = self.urlAPI + '/' + ta.id;
    return this.http.delete(
      url
    ).toPromise()
  }
}
