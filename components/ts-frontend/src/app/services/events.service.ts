import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {TextAnnotation} from '../models/text-annotation';

@Injectable()
export class EventsService {
  private subjectHighlightLayer = new Subject<string>();
  observableHighlightLayer: Observable<string>;

  private subjectFocusTextAnnotation = new Subject<TextAnnotation>();
  observableFocusTextAnnotation: Observable<TextAnnotation>;

  constructor() {
    this.observableHighlightLayer = this.subjectHighlightLayer;
    this.observableFocusTextAnnotation = this.subjectFocusTextAnnotation;
  }

  sendHightlightLayer(layerId: string) {
    this.subjectHighlightLayer.next(layerId);
  }

  focusTextAnnotation(ta: TextAnnotation) {
    this.subjectFocusTextAnnotation.next(ta);
  }

}
