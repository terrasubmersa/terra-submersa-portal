import {Injectable} from '@angular/core';
import {MapLayerDescription} from '../models/map-layer-description';
import {AvailableMapLayers} from '../models/available-map-layers';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import * as _ from 'lodash';
import {TileSystemBoundaries} from '../models/tile-system-boundaries';

@Injectable()
export class DataLayersService {
  private baseLayer: MapLayerDescription;

  private amlCache: AvailableMapLayers;

  constructor(private http: HttpClient) {
    this.baseLayer = new MapLayerDescription(
      'openstreetmap.landscape',
      'https://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png?apikey=8ec51d8024c2405fb9bc825df102046a',
      'OpenStreet map landscape',
      'OpenStreet map landscape',
      'OpenStreetMap, Imagery, MapBox',
      new TileSystemBoundaries(1, 19, undefined)
    )
  }

  getMapLayerDescriptionList(): Promise<AvailableMapLayers> {
    const self = this;
    if (self.amlCache !== undefined) {
      return new Promise((resolve) => resolve(this.amlCache))

    }

    return this.http.get(environment.urlTileBackend + '/api/systems')
      .toPromise()
      .then(res => {
        const layers = _.map(res, (resObj: any) => {
          return new MapLayerDescription(
            resObj.id,
            environment.apiOSM + resObj.urlMask,
            resObj.title,
            resObj.description,
            resObj.copyright,
            resObj.boundaries as TileSystemBoundaries
          )
        });
        layers.unshift(self.baseLayer);
        self.amlCache = new AvailableMapLayers(layers)
        return self.amlCache;
      });
    // .catch((err) => console.error(err));
  }

  getMapLayerIds(): Promise<string[]> {
    return this.getMapLayerDescriptionList()
      .then((aml) => aml.list.map(l => l.id));
  }
}
