import {Component} from '@angular/core';
import {DataLayersService} from './services/data-layers.service';
import * as TextAnnotationsActions from './store/text-annotations-actions';
import * as SelectedMapLayersActions from './store/selected-map-layers-actions';
import {Store} from '@ngrx/store';
import {AppState} from './AppState';
import {AvailableMapLayers} from './models/available-map-layers';
import {Angulartics2GoogleAnalytics} from 'angulartics2/ga';
import {environment} from '../environments/environment';
import {Angulartics2} from 'angulartics2';
import {TextAnnotationService} from './services/text-annotation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DataLayersService, TextAnnotationService]
})
export class AppComponent {
  title = 'Terra Submersa data viewer';

  constructor(private dataLayersService: DataLayersService,
              private textAnnotationService: TextAnnotationService,
              private store: Store<AppState>,
              angulartics2: Angulartics2,
              angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics) {
    dataLayersService.getMapLayerDescriptionList()
      .then((aml: AvailableMapLayers) => {
        this.store.dispatch(new SelectedMapLayersActions.SetAvailableLayers(aml));
        this.store.dispatch(new SelectedMapLayersActions.SelectAll());
      });
    textAnnotationService.getList()
      .then((tas) =>
        this.store.dispatch(new TextAnnotationsActions.SetAvailableTextAnnotations(tas))
      );

    if (!environment.production) {
      angulartics2.developerMode(true);
    }
  }
}
