import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MapBaseComponent} from './components/map-base/map-base.component';
import {TsFooterComponent} from './components/ts-footer/ts-footer.component';
import {TsHeaderComponent} from './components/ts-header/ts-header.component';
import {MapLayerSelectorComponent} from './components/map-layer-selector/map-layer-selector.component';
import 'hammerjs';
import {MenuActionsComponent} from './components/menu-actions/menu-actions.component';
import {importedModules} from './imported-modules';
import {CursorToolComponent} from './components/cursor-tool/cursor-tool.component';
import {TileSystemDetailsComponent} from './components/tile-system-details/tile-system-details.component';
import {TextAnnotationLayerComponent} from './components/text-annotation-layer/text-annotation-layer.component';
import {TextAnnotationSelectorComponent} from './components/text-annotation-selector/text-annotation-selector.component';
import {SideNavContainerComponent} from './components/side-nav-container/side-nav-container.component';
import {EventsService} from './services/events.service';
import { DetailsTextAnnotationComponent } from './components/details-text-annotation/details-text-annotation.component';
import { SideDrawerContentComponent } from './components/side-drawer-content/side-drawer-content.component';
import { TextAnnotationDetailsComponent } from './components/text-annotation-details/text-annotation-details.component';
import { LayerCoordinatesComponent } from './components/layer-coordinates/layer-coordinates.component';

@NgModule({
  declarations: [
    AppComponent,
    CursorToolComponent,
    MapBaseComponent,
    TsFooterComponent,
    TsHeaderComponent,
    MapLayerSelectorComponent,
    MenuActionsComponent,
    CursorToolComponent,
    TileSystemDetailsComponent,
    TextAnnotationLayerComponent,
    TextAnnotationSelectorComponent,
    SideNavContainerComponent,
    DetailsTextAnnotationComponent,
    SideDrawerContentComponent,
    TextAnnotationDetailsComponent,
    LayerCoordinatesComponent
  ],
  imports: importedModules,
  entryComponents: [
    MapLayerSelectorComponent
  ],

  providers: [EventsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
