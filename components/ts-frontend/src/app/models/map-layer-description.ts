import {TileSystemBoundaries} from './tile-system-boundaries';

export class MapLayerDescription {
  readonly id: string;
  readonly osmPath: string;
  readonly title: string;
  readonly description: string;
  readonly copyright: string;
  readonly boundaries: TileSystemBoundaries

  constructor(id: string, osmPath: string, title: string, description: string, copyright: string, boundaries: TileSystemBoundaries) {
    this.id = id;
    this.osmPath = osmPath;
    this.title = title;
    this.description = description;
    this.copyright = copyright;
    this.boundaries = boundaries
  }
}
