import {TextAnnotationSet} from './text-annotation-set';
import {TextAnnotation} from './text-annotation';
import {LatLonCoords} from './lat-lon-coord';

describe('TextAnnotationSet', () => {
  const ta_a = new TextAnnotation(
    'id_a',
    'text a',
    new LatLonCoords(12.34, 56.78),
    'system_0',
    ['tag_a', 'tag_b']
  );
  const ta_a_bis = new TextAnnotation(
    'id_a',
    'text a bis',
    new LatLonCoords(12.34, 56.78),
    'system_0',
    ['tag_a', 'tag_b']
  );
  const ta_b = new TextAnnotation(
    'id_b',
    'text b',
    new LatLonCoords(12.34, 56.78),
    'system_0',
    ['tag_a', 'tag_b']
  );
  const ta_c = new TextAnnotation(
    'id_c',
    'text c',
    new LatLonCoords(12.34, 56.78),
    'system_0',
    ['tag_a', 'tag_b']
  );

  it('size', () => {
    const set = new TextAnnotationSet([ta_a, ta_b]);

    const len = set.size();

    expect(len).toBe(2);
  });

  const getIds = (set: TextAnnotationSet) => set.getAvailable().map((t) => t.id);
  const getTexts = (set: TextAnnotationSet) => set.getAvailable().map((t) => t.text);

  it('update new', () => {
    const set = new TextAnnotationSet([ta_a, ta_b]);

    const set_2 = set.update(ta_c);

    expect(getIds(set_2)).toEqual(['id_a', 'id_b', 'id_c']);
  });

  it('update existing first', () => {
    const set = new TextAnnotationSet([ta_a, ta_b, ta_c]);

    const set_2 = set.update(ta_a_bis);

    expect(getTexts(set_2)).toEqual(['text a bis', 'text b', 'text c']);
  });

  it('update existing middle', () => {
    const set = new TextAnnotationSet([ta_b, ta_a, ta_c]);

    const set_2 = set.update(ta_a_bis);

    expect(getTexts(set_2)).toEqual(['text b', 'text a bis', 'text c']);
  });
  it('update existing end', () => {
    const set = new TextAnnotationSet([ta_b, ta_c, ta_a]);

    const set_2 = set.update(ta_a_bis);

    expect(getTexts(set_2)).toEqual(['text b', 'text c', 'text a bis']);
  });

  it('remove existing end', () => {
    const set = new TextAnnotationSet([ta_b, ta_c, ta_a]);

    const set_2 = set.remove(ta_c);

    expect(getTexts(set_2)).toEqual(['text b', 'text a']);
  });
});
