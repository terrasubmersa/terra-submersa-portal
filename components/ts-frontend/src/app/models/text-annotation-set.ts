import {TextAnnotation} from './text-annotation';
import * as _ from 'lodash';

export class TextAnnotationSet {

  constructor(private availableTextAnnotations: TextAnnotation[] = [],
              private visibility: boolean = true) {
  }

  size(): number {
    return this.availableTextAnnotations.length;
  }

  getAvailable(): TextAnnotation[] {
    return this.availableTextAnnotations;
  }

  setAvailable(annotations: TextAnnotation[]) {
    return new TextAnnotationSet(annotations, this.visibility);
  }

  setVisibility(isVisible: boolean) {
    return new TextAnnotationSet(this.availableTextAnnotations, isVisible);
  }

  getVisibility() {
    return this.visibility;
  }

  /**
   * update a text annotation.
   * i.e. append it it none exist with the same id
   * or replace it
   * @param {TextAnnotation} textAnnotation
   * @return {TextAnnotationSet}
   */
  update(textAnnotation: TextAnnotation): TextAnnotationSet {
    const self = this;
    const i = _.findIndex(self.availableTextAnnotations, (ta) => _.isEqual(ta.id, textAnnotation.id));

    if (i < 0) {
      return new TextAnnotationSet(
        self.availableTextAnnotations.concat(textAnnotation),
        self.visibility
      );
    } else {
      return new TextAnnotationSet(
        _.concat(self.availableTextAnnotations.slice(0, i), textAnnotation, self.availableTextAnnotations.slice(i+1)),
        self.visibility
      );
    }
  }

  /**
   * update a text annotation.
   * i.e. append it it none exist with the same id
   * or replace it
   * @param {TextAnnotation} textAnnotation
   * @return {TextAnnotationSet}
   */
  remove(textAnnotation: TextAnnotation): TextAnnotationSet {
    const self = this;
    const i = _.findIndex(self.availableTextAnnotations, (ta) => _.isEqual(ta.id, textAnnotation.id));

    if (i < 0) {
      return this
    } else {
      return new TextAnnotationSet(
        _.concat(self.availableTextAnnotations.slice(0, i), self.availableTextAnnotations.slice(i+1)),
        self.visibility
      );
    }
  }
}
