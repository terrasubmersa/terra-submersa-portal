import {LatLonCoords} from './lat-lon-coord';
import {LatLonBBox} from './lat-lon-bbox';

export class TileSystemBoundaries {
  readonly minLevel: number;
  readonly maxLevel: number;
  readonly bbox: LatLonBBox;

  constructor(minLevel: number, maxLevel: number, bbox: LatLonBBox) {
    this.minLevel = minLevel;
    this.maxLevel = maxLevel;
    this.bbox = bbox;
  }
}
