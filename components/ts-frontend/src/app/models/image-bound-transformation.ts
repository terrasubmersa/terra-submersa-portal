/**
 * Describes the transformation applied to an image, with a center (LatLonCoords), latitude on longitude widths and a rotation.
 * Beside being a pojo, the object can also return position along its sides (top, right, bottom, left) with a weight factorw.
 * w=0 point to the side middle, -1/+1 to the two corners (respectively bottom/top or left/right)
 */
import {LatLonCoords} from './lat-lon-coord';
import {LatLonCoordsCalculatorService} from '../services/lat-lon-coords-calculator.service';

const calculatorService = new LatLonCoordsCalculatorService();

export class ImageBoundTransformation {
  readonly center: LatLonCoords;
  // latitude
  readonly widthLat: number;
  // longitude
  readonly widthLon: number;
  readonly rotation: number;


  constructor(center: LatLonCoords, widthLat: number, widthLon: number, rotation: number) {
    this.center = center;
    this.widthLat = widthLat;
    this.widthLon = widthLon;
    this.rotation = rotation
  }

  /**
   * return a LatLonCoords along the top side.
   * -1=> the upperLeft corner
   * 1 => the upperRight
   * 0 => the middle
   * @param {number} w
   */
  pointTop(w: number): LatLonCoords {
    const llc0 = new LatLonCoords(
      this.center.lat + this.widthLat / 2,
      this.center.lon + w * this.widthLon / 2
    );
    return calculatorService.rotateWithCenter(this.center, this.rotation, llc0);
  }

  pointRight(w: number): LatLonCoords {
    const llc0 = new LatLonCoords(
      this.center.lat + w * this.widthLat / 2,
      this.center.lon + this.widthLon / 2
    );
    return calculatorService.rotateWithCenter(this.center, this.rotation, llc0);
  }

  pointBottom(w: number): LatLonCoords {
    const llc0 = new LatLonCoords(
      this.center.lat - this.widthLat / 2,
      this.center.lon + w * this.widthLon / 2
    );
    return calculatorService.rotateWithCenter(this.center, this.rotation, llc0);
  }

  pointLeft(w: number): LatLonCoords {
    const llc0 = new LatLonCoords(
      this.center.lat + w * this.widthLat / 2,
      this.center.lon - this.widthLon / 2
    );
    return calculatorService.rotateWithCenter(this.center, this.rotation, llc0);
  }
}
