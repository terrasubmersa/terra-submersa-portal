import {AvailableMapLayers} from './available-map-layers';
/**
 * handles the selected map layers.
 * We live in a immutable world, so all setter wil return a new object
 *
 * Created by alex on 16.09.17.
 */
export class SelectedMapLayers {
  readonly selectedIds: Set<string>;
  readonly availableMapLayers: AvailableMapLayers;

  constructor(selectedIds: Set<string> = new Set(), availableMapLayers: AvailableMapLayers = new AvailableMapLayers([])) {
    this.selectedIds = selectedIds;
    this.availableMapLayers = availableMapLayers;
  }

  /**
   * return a new object with altered availableMapLayers
   * @param availableMapLayers
   * @return {SelectedMapLayers}
   */
  setAvailable(availableMapLayers: AvailableMapLayers) {
    return new SelectedMapLayers(this.selectedIds, availableMapLayers)
  }

  isSelected(id: string) {
    return this.selectedIds.has(id);
  }

  /**
   * return a new object with an added selected id
   * @param id
   * @return {Set<string>}
   */
  select(id: string) {
    this.selectedIds.add(id);
    return new SelectedMapLayers(this.selectedIds, this.availableMapLayers);
  }
  /**
   * return a new object with a removed selected id
   * @param id
   * @return {Set<string>}
   */
  unselect(id: string) {
    this.selectedIds.delete(id);
    return new SelectedMapLayers(this.selectedIds, this.availableMapLayers);
  }

  /**
   * return a new object with all available map layers selected
   */
  selectAll() {
    const self = this;
    const s = new Set<string>();

    self.availableMapLayers.list.forEach((l) => s.add(l.id));
    return new SelectedMapLayers(s, this.availableMapLayers);
  }
}
