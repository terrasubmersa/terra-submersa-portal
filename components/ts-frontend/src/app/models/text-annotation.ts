import {LatLonCoords} from './lat-lon-coord';

export class TextAnnotation {
  /*
  {
  "id": "5a2026918202001203ec2b42",
  "text": "paf le chien",
  "position": {
    "lat": 37.4269307,
    "lon": 23.1331932
  },
  "tileSystemId": "sensys-JULY_18_A",
  "labels": [
    "lab one"
  ]
}
   */
  readonly id: string;
  readonly text: string;
  readonly position: LatLonCoords;
  readonly tileSystemId: string;
  readonly labels: string[];

  constructor(id: string,
              text: string,
              position: LatLonCoords,
              tileSystemId: string,
              labels: string[]) {
    this.id = id;
    this.text = text;
    this.position = position;
    this.tileSystemId = tileSystemId;
    this.labels = labels
  }

}
