import {async, TestBed} from '@angular/core/testing';

import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';
import {selectedMapLayersReducer} from './store/selected-map-layers-reducer';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatMenuModule, MatSlideToggleModule, MatIconModule} from '@angular/material';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {APP_BASE_HREF} from '@angular/common';
import {Angulartics2Module} from 'angulartics2';
import {Angulartics2GoogleAnalytics} from 'angulartics2/ga';
import {RouterModule, Routes} from '@angular/router';

describe('AppComponent', () => {
  beforeEach(async(() => {
    const ROUTES: Routes = [
    ];

    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'}
      ],
      imports: [
        RouterModule.forRoot(ROUTES),
        StoreModule.forRoot({
          selectedMapLayers: selectedMapLayersReducer,
        }),
        HttpClientTestingModule,
        MatMenuModule,
        MatButtonModule,
        MatSlideToggleModule,
        MatIconModule,
        NoopAnimationsModule,
        Angulartics2Module.forRoot([Angulartics2GoogleAnalytics]),

      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Terra Submersa data viewer');
  }));
});
