import {Component, NgZone, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {SelectedMapLayers} from '../../models/selected-map-layers';
import {Store} from '@ngrx/store';
import {AppState} from '../../AppState';
import {MatDialogRef} from '@angular/material';
import * as SelectedMapLayersActions from '../../store/selected-map-layers-actions';
import {MapLayerDescription} from '../../models/map-layer-description';
import {EventsService} from '../../services/events.service';

@Component({
  selector: 'ts-map-layer-selector',
  templateUrl: './map-layer-selector.component.html',
  styleUrls: ['./map-layer-selector.component.css'],
  providers:[]
})
export class MapLayerSelectorComponent implements OnInit {
  obsSelectedMapLayers: Observable<SelectedMapLayers>;
  selectedMapLayers: SelectedMapLayers;
  selectedIds = {};
  focusMapLayerDescription: MapLayerDescription;

  constructor(public dialogRef: MatDialogRef<MapLayerSelectorComponent>,
              private store: Store<AppState>,
              private eventsService: EventsService,
              private zone: NgZone) {
    this.obsSelectedMapLayers = store.select('selectedMapLayers');
  }

  ngOnInit() {
    const self = this;

    self.obsSelectedMapLayers.subscribe((sml) => {
      self.selectedMapLayers = sml;
      self.selectedMapLayers.selectedIds.forEach((id) => self.selectedIds[id] = true);
    })
  }

  mouseInMapLayer(mld: MapLayerDescription) {
    this.eventsService.sendHightlightLayer(mld.id);
    this.focusMapLayerDescription = mld;
  }

  mouseOutMapLayer() {
    this.eventsService.sendHightlightLayer(undefined);
    this.focusMapLayerDescription = undefined;
  }

  changeToggle(id, checked) {
    this.store.dispatch(new SelectedMapLayersActions.SelectState(id, checked));
  }

}
