import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MapLayerSelectorComponent} from './map-layer-selector.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {importedModules} from '../../imported-modules';
import {APP_BASE_HREF} from '@angular/common';
import {EventsService} from '../../services/events.service';

class MatDialogRefMock {
}

describe('MapLayerSelectorComponent', () => {
  let component: MapLayerSelectorComponent;
  let fixture: ComponentFixture<MapLayerSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapLayerSelectorComponent ],
      imports: importedModules,
      providers: [
        EventsService,
        { provide: MatDialogRef, useClass: MatDialogRefMock },
        { provide: APP_BASE_HREF, useValue : '/' }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapLayerSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
