import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {TextAnnotation} from '../../models/text-annotation';
import {TextAnnotationService} from '../../services/text-annotation.service';
import {DataLayersService} from '../../services/data-layers.service';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material';
import {AppState} from '../../AppState';
import {Store} from '@ngrx/store';
import * as _ from 'lodash';
import * as TextAnnotationsActions from '../../store/text-annotations-actions';
import {EventsService} from '../../services/events.service';

function isTextLess(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const forbidden = control.value.trim() === '';
    return forbidden ? {'isTextLess': {value: control.value}} : null;
  };
}


@Component({
  selector: 'ts-text-annotation-details',
  templateUrl: './text-annotation-details.component.html',
  styleUrls: ['./text-annotation-details.component.css']
})
export class TextAnnotationDetailsComponent implements OnInit {
  @Input() textAnnotation: TextAnnotation;
  @Input() whenCompletedAction: Function;

  taForm: FormGroup;
  tileSystemIds: string[];
  labels: string[];

  isRemoveWaitingConfirmation = false;

  separatorKeysCodes = [ENTER, COMMA];

  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = true;

  constructor(private fb: FormBuilder,
              private textAnnotationService: TextAnnotationService,
              private eventsService: EventsService,
              private dataLayersService: DataLayersService,
              private store: Store<AppState>,) {
  }

  ngOnInit() {
    const self = this;
    self.dataLayersService.getMapLayerIds()
      .then(xs => self.tileSystemIds = xs)
      .catch((err) => console.error(err));

    self.labels = _.clone(self.textAnnotation.labels);
    self.taForm = self.fb.group({
      text: new FormControl(self.textAnnotation.text,
      [
        isTextLess()
      ]),
      tileSystemId: new FormControl(self.textAnnotation.tileSystemId, [
        Validators.required,
      ]),
      labels: self.fb.array(self.labels)
    });
  }


  currentFormTextnnotation() {
    const self = this;
    const v = self.taForm.value;
    return new TextAnnotation(
      self.textAnnotation.id,
      v.text.trim(),
      self.textAnnotation.position,
      v.tileSystemId,
      v.labels
    )
  }

  update() {
    const self = this;

    if(!self.taForm.valid){
      return;
    }

    const ta = self.currentFormTextnnotation();
    self.textAnnotationService.update(ta)
      .then((json) => {
        const taFromServer = json as TextAnnotation;
        self.whenCompletedAction();
        return taFromServer
      })
      .then((taFromServer) => {
        this.store.dispatch(new TextAnnotationsActions.UpdateTextAnnotation(taFromServer))
      })
  }

  cancel() {
    const self = this;
    self.whenCompletedAction()
  }

  remove() {
    const self = this;
    const ta = self.currentFormTextnnotation();
    if (self.isRemoveWaitingConfirmation) {
      self.textAnnotationService.remove(ta)
        .then((json) => {
          self.whenCompletedAction()
          self.isRemoveWaitingConfirmation = false;
        })
        .then(() => {
          this.store.dispatch(new TextAnnotationsActions.RemoveTextAnnotation(ta))
        })
        .catch((err) => console.error('error while removing', err));
      return;
    }
    self.isRemoveWaitingConfirmation = true;
    setTimeout(() => self.isRemoveWaitingConfirmation = false, 2000);
  }

  addChipLabel(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.taForm.value.labels.push(value.trim());
      this.labels.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeChipLabel(label: any): void {
    const index = this.taForm.value.labels.indexOf(label);

    if (index >= 0) {
      this.taForm.value.labels.splice(index, 1);
      this.labels.splice(index, 1);
    }
  }

  mouseInMapLayer(id: string) {
    this.eventsService.sendHightlightLayer(id);
  }

  mouseOutMapLayer() {
    this.eventsService.sendHightlightLayer(undefined);
  }
}
