import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SideDrawerContentComponent} from './side-drawer-content.component';
import {TextAnnotationDetailsComponent} from '../text-annotation-details/text-annotation-details.component';
import {importedModules} from '../../imported-modules';
import {CUSTOM_ELEMENTS_SCHEMA, ElementRef} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import {EventsService} from '../../services/events.service';
import {MatDrawer} from '@angular/material';

class MockElementRef implements ElementRef {
  nativeElement = {};
}

describe('SideDrawerContentComponent', () => {
  let component: SideDrawerContentComponent;
  let fixture: ComponentFixture<SideDrawerContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SideDrawerContentComponent,
        TextAnnotationDetailsComponent
      ],
      imports: importedModules,
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        MatDrawer,
        {provide: ElementRef, useClass: MockElementRef},
        EventsService,
        {provide: APP_BASE_HREF, useValue: '/'}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideDrawerContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
