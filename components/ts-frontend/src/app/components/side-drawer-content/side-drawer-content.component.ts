import {Component, Injector, Input, OnInit} from '@angular/core';
import {TextAnnotation} from '../../models/text-annotation';
import {EventsService} from '../../services/events.service';
import {MatDrawer} from '@angular/material';

@Component({
  selector: 'ts-side-drawer-content',
  templateUrl: './side-drawer-content.component.html',
  styleUrls: ['./side-drawer-content.component.css']
})
export class SideDrawerContentComponent implements OnInit {
  textAnnotation: TextAnnotation;
  sidenav: any;

  constructor(private eventsService: EventsService,
              private inj: Injector) {
    const self = this;
    self.sidenav = this.inj.get(MatDrawer)
  }

  ngOnInit() {
    const self = this;
    self.eventsService.observableFocusTextAnnotation.subscribe((ta) => {
      self.textAnnotation = ta;
      self.sidenav.open();
    });
  }


  /**
   * called upon text anjotation completion
   * for whatever reason, the method is applied on the children element if not defined as a variable...
   */
  onCompletedTextAnnotation = () => {
    const self = this;
    self.textAnnotation = undefined;
    self.sidenav.close()
  }
}
