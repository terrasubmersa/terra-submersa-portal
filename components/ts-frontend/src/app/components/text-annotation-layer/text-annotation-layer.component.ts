import {Component, ComponentFactoryResolver, Input, OnInit} from '@angular/core';
import {TextAnnotationSet} from '../../models/text-annotation-set';
import * as L from 'leaflet';
import * as _ from 'lodash';
import {Observable} from 'rxjs/Observable';
import {AppState} from '../../AppState';
import {Store} from '@ngrx/store';
import {Icon, LayerGroup, Marker} from 'leaflet';
import {SelectedMapLayers} from '../../models/selected-map-layers';
import {TextAnnotation} from '../../models/text-annotation';
import {EventsService} from '../../services/events.service';

class MarkerTextAnnotation {
  readonly textAnnotation: TextAnnotation;
  readonly marker: Marker;

  constructor(marker: Marker, textAnnotation: TextAnnotation) {
    this.marker = marker;
    this.textAnnotation = textAnnotation
  }
}

@Component({
  selector: 'ts-text-annotation-layer',
  templateUrl: './text-annotation-layer.component.html',
  styleUrls: ['./text-annotation-layer.component.css'],
})
export class TextAnnotationLayerComponent implements OnInit {
  @Input() map: any;
  obsTextAnnotationSet: Observable<TextAnnotationSet>;
  obsSelectedMapLayers: Observable<SelectedMapLayers>;
  layerGroup: LayerGroup;
  selectedIds: string[] = [];
  textAnnotationSet: TextAnnotationSet;
  dictMarkers: Map<String, MarkerTextAnnotation> = new Map();

  iconAnnotationPresent = new Icon({
    iconUrl: 'assets/icons/ic_place_accent_48.png',
    iconSize: [32, 32],
    iconAnchor: [16, 31]
  });

  constructor(private store: Store<AppState>,
              private eventsService: EventsService) {
    const self = this;
    self.obsTextAnnotationSet = store.select('textAnnotations');
    this.obsSelectedMapLayers = store.select('selectedMapLayers');
  }

  ngOnInit() {
    const self = this;
    self.layerGroup = new LayerGroup([])
      .addTo(self.map);

    self.obsTextAnnotationSet.subscribe((tas) => {
      self.textAnnotationSet = tas;
      self.display();

    });
    self.obsSelectedMapLayers.subscribe((sml) => {
      self.selectedIds = Array.from(sml.selectedIds);
      self.display();
    })
  }

  private htmlPopup(ta: TextAnnotation) {
    const self = this;

    const container = L.DomUtil.create('div');
    const btn = L.DomUtil.create('div', 'text-annotation-popup menu edit', container);
    L.DomEvent.on(btn, 'click', () => {
      self.eventsService.focusTextAnnotation(ta);
    });

    L.DomUtil.create('div', 'text-annotation-popup text', container).innerText = ta.text;
    const divLabels = L.DomUtil.create('div', 'text-annotation-popup label-group', container);
    for (let i = ta.labels.length - 1; i >= 0; i--) {
      const lab = ta.labels[i];
      L.DomUtil.create('div', 'label', divLabels).innerText = lab;
    }
    L.DomUtil.create('div', 'text-annotation-popup tile-system-id', container).innerText = ta.tileSystemId;

    return container;
  }

  display() {
    const self = this;
    if (!self.textAnnotationSet.getVisibility()) {
      self.layerGroup.clearLayers();
      self.dictMarkers.clear();
      return;
    }
    const isFileStemId = {};


    const orphanMarkers = new Set<String>();
    const allTAIds = new Set(_.map(self.textAnnotationSet.getAvailable(), 'id'))
    for (let mat of Array.from(self.dictMarkers.values())) {
      const id = mat.textAnnotation.id;
      if (!allTAIds.has(id)) {
        self.dictMarkers.get(id).marker.remove();
        self.dictMarkers.delete(id)
      }
    }

    self.selectedIds.forEach((id) => isFileStemId[id] = true);
    self.textAnnotationSet.getAvailable().forEach((ta) => {
      if (!isFileStemId[ta.tileSystemId]) {
        if (self.dictMarkers.has(ta.id)) {
          self.dictMarkers.get(ta.id).marker.remove();
          self.dictMarkers.delete(ta.id);
        }
        return;
      }
      if (self.dictMarkers.has(ta.id) && _.isEqual(self.dictMarkers.get(ta.id).textAnnotation, ta)) {
        // marker is already present and with the same annotation
        return;
      }
      let isPopuOpen = false;
      if (self.dictMarkers.has(ta.id)) {
        isPopuOpen = self.dictMarkers.get(ta.id).marker.isPopupOpen();
        self.dictMarkers.get(ta.id).marker.remove();
      }
      const marker = new Marker([ta.position.lat, ta.position.lon], {icon: self.iconAnnotationPresent}).bindPopup(self.htmlPopup(ta));
      self.dictMarkers.set(ta.id, new MarkerTextAnnotation(marker, ta));
      self.layerGroup.addLayer(marker);
      if (isPopuOpen) {
        marker.openPopup();
      }
    });
  }
}
