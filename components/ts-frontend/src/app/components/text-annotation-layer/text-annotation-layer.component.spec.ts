import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TextAnnotationLayerComponent} from './text-annotation-layer.component';
import {importedModules} from '../../imported-modules';
import {APP_BASE_HREF} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

// describe('TextAnnotationLayerComponent', () => {
//   let component: TextAnnotationLayerComponent;
//   let fixture: ComponentFixture<TextAnnotationLayerComponent>;
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [TextAnnotationLayerComponent],
//       imports: importedModules,
//       schemas: [CUSTOM_ELEMENTS_SCHEMA],
//       providers: [
//         {provide: APP_BASE_HREF, useValue: '/'}
//       ]
//     })
//       .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(TextAnnotationLayerComponent);
//     component = fixture.componentInstance;
//     component.map = true;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
