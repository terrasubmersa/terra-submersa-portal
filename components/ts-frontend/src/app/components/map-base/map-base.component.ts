import {Component, OnInit} from '@angular/core';
import * as L from 'leaflet';
import {LatLng, LatLngBounds, TileLayer} from 'leaflet';
import {Observable} from 'rxjs/Observable';
import {AppState} from '../../AppState';
import {Store} from '@ngrx/store';
import {SelectedMapLayers} from '../../models/selected-map-layers';
import {MapLayerDescription} from '../../models/map-layer-description';
import {EventsService} from '../../services/events.service';

@Component({
  selector: 'ts-map-base',
  templateUrl: './map-base.component.html',
  styleUrls: ['./map-base.component.css'],
  providers: []
})
export class MapBaseComponent implements OnInit {
  map;
  osbSelectedMapLayers: Observable<SelectedMapLayers>;
  obsHighligthLayerId: Observable<string>;

  leafletMapLayers: Map<string, TileLayer> = new Map();

  constructor(private store: Store<AppState>,
              private eventsService: EventsService) {
    this.osbSelectedMapLayers = store.select('selectedMapLayers');
    this.obsHighligthLayerId = store.select('mapLayers');
  }

  ngOnInit() {
    const self = this;
    self.map = L.map('mapid');
    self.map.setView([37.42, 23.12], 14);

    L.control.scale({metric: true, position: 'bottomright'}).addTo(self.map);

    self.osbSelectedMapLayers.subscribe((xs) => {
      this.updateSelectedMapLayers(xs);
    });

    this.eventsService.observableHighlightLayer.subscribe((id) => self.highlightLayer(id),
      (err) => console.error('error subscribing to observableHighlightLayer', err)
    )

  }

  updateSelectedMapLayers(sml: SelectedMapLayers) {
    const self = this;

    sml.availableMapLayers.list.forEach((ld) => {
      if (self.leafletMapLayers.has(ld.id)) {
        self.removeMapLayer(ld.id);
      }
    });
    sml.availableMapLayers.list.forEach((ld) => {
      if (sml.isSelected(ld.id)) {
        self.addMapLayer(ld);
      }
    });

  }

  getSelectedMapLayers(): Map<string, any> {
    return this.leafletMapLayers;
  }

  addMapLayer(ld: MapLayerDescription) {
    const self = this;

    let bounds: LatLngBounds;
    if (ld.boundaries.bbox !== undefined) {
      const southWest = new LatLng(ld.boundaries.bbox.lowerRight.lat, ld.boundaries.bbox.upperLeft.lon);
      const northEast = new LatLng(ld.boundaries.bbox.upperLeft.lat, ld.boundaries.bbox.lowerRight.lon);
      bounds = new LatLngBounds(southWest, northEast);
    }

    self.leafletMapLayers.set(ld.id, L.tileLayer(ld.osmPath, {
      maxZoom: ld.boundaries.maxLevel,
      attribution: ld.copyright,
      id: ld.id,
      bounds: bounds
    }));
    self.leafletMapLayers.get(ld.id).addTo(self.map);
  }

  removeMapLayer(id: string) {
    const self = this;

    self.map.removeLayer(self.leafletMapLayers.get(id));
    self.leafletMapLayers.delete(id);
  }

  highlightLayer(id: string) {
    const self = this;

    const layers = Array.from(self.leafletMapLayers.values());

    if (id === undefined || self.leafletMapLayers.get(id) === undefined) {
      for (const l of layers) {
        l.setOpacity(1)
      }
      return;
    }
    for (const l of layers) {
      l.setOpacity(0.2)
    }
    self.leafletMapLayers.get(id).setOpacity(1.0);
  }
}

