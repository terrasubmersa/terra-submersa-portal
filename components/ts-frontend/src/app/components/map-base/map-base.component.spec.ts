import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MapBaseComponent} from './map-base.component';
import {MapLayerDescription} from '../../models/map-layer-description';
import {importedModules} from '../../imported-modules';
import {TileSystemBoundaries} from '../../models/tile-system-boundaries';
import {CursorToolComponent} from '../cursor-tool/cursor-tool.component';
import {APP_BASE_HREF} from '@angular/common';
import {TextAnnotationLayerComponent} from '../text-annotation-layer/text-annotation-layer.component';
import {EventsService} from '../../services/events.service';
import {LayerCoordinatesComponent} from '../layer-coordinates/layer-coordinates.component';

describe('MapBaseComponent', () => {
  let component: MapBaseComponent;
  let fixture: ComponentFixture<MapBaseComponent>;

  const mapLayerDescription1 = new MapLayerDescription('id.0', 'http://pipo', 'title.0', 'descr.0', 'attr.0', new TileSystemBoundaries(12, 19, undefined));
  const mapLayerDescription2 = new MapLayerDescription('id.1', 'http://paf', 'title.1', 'descr.1', 'attr.1', new TileSystemBoundaries(12, 19, undefined));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MapBaseComponent,
        CursorToolComponent,
        TextAnnotationLayerComponent,
        LayerCoordinatesComponent
      ],
      imports: importedModules,
      providers: [
        EventsService,
        {provide: APP_BASE_HREF, useValue: '/'}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have not map layers', () => {
    expect(component.getSelectedMapLayers().size).toBe(0);
  });


  it('add 2 layers and check size', () => {
    component.addMapLayer(mapLayerDescription1);
    component.addMapLayer(mapLayerDescription2);

    const n = component.getSelectedMapLayers().size;

    expect(n).toBe(2);
  });

  it('add 2 layers, remove 1 and check size', () => {
    component.addMapLayer(mapLayerDescription1);
    component.addMapLayer(mapLayerDescription2);
    component.removeMapLayer('id.0');

    const n = component.getSelectedMapLayers().size;

    expect(n).toBe(1);
  });
});
