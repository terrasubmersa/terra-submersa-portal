import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavContainerComponent } from './side-nav-container.component';
import {APP_BASE_HREF} from '@angular/common';
import {EventsService} from '../../services/events.service';
import {CUSTOM_ELEMENTS_SCHEMA, ElementRef} from '@angular/core';
import {MatDrawer} from '@angular/material';
import {importedModules} from '../../imported-modules';

class MockElementRef implements ElementRef {
  nativeElement = {};
}

describe('SideNavContainerComponent', () => {
  let component: SideNavContainerComponent;
  let fixture: ComponentFixture<SideNavContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideNavContainerComponent ],
      imports: importedModules,
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        MatDrawer,
        {provide: ElementRef, useClass: MockElementRef},
        EventsService,
        {provide: APP_BASE_HREF, useValue: '/'}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
