import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material';

@Component({
  selector: 'ts-side-nav-container',
  templateUrl: './side-nav-container.component.html',
  styleUrls: ['./side-nav-container.component.css']
})
export class SideNavContainerComponent implements OnInit {
  @ViewChild('sidenav') private sidenav: MatSidenav;

  constructor() {
  }

  ngOnInit() {
  }

}
