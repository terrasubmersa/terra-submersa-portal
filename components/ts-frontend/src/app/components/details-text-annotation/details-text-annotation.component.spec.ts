import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsTextAnnotationComponent } from './details-text-annotation.component';

describe('DetailsTextAnnotationComponent', () => {
  let component: DetailsTextAnnotationComponent;
  let fixture: ComponentFixture<DetailsTextAnnotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsTextAnnotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsTextAnnotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
