import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import * as L from 'leaflet';
import * as d3 from 'd3';
import {scaleLinear} from "d3-scale";
import {Point} from 'leaflet';


class LayerCoordinates extends L.Layer {
  private svg;
  private xScale;
  private xAxis;
  private yScale;
  private yAxis;

  private axisLeftMargin = 60;
  private axisRightMargin = 80;
  private axisTopMargin = 30;
  private axisBottomMargin = 80;

  onAdd(map: L.Map): this {
    const self = this;
    self.svg = d3.select(map.getPane('markerPane'))
      .append('svg');

    self.xScale = d3.scaleLinear();
    self.yScale = d3.scaleLinear();
    self.updateScale(map);
    self.xAxis = d3.axisTop(self.xScale).ticks(5);
    self.yAxis = d3.axisRight(self.yScale).ticks(5);
    const dim = map.getSize();
    self.svg.append('g')
      .attr('class', 'x-axis axis')
      .attr('transform', 'translate(0, ' + self.axisTopMargin + ')')
      .call(self.xAxis);
    self.svg.append('g')
      .attr('class', 'y-axis axis')
      .attr('transform', 'translate(' + (dim.x-self.axisRightMargin) + ', 0)')
      .call(self.yAxis);

    map.on('move', (e) => self.redraw(e));
    map.on('load', (e) => self.redraw(e));
    map.on('resize', (e) => self.redraw(e));
    map.on('viewreset', (e) => self.redraw(e));
    return self;
  }

  updateScale(map: L.Map) {
    const self = this;
    const llBounds = map.getBounds();
    const dim = map.getSize();

    const llUpperLeft = map.containerPointToLatLng(new Point(self.axisLeftMargin, self.axisTopMargin));
    const llLowerRight = map.containerPointToLatLng(new Point(dim.x - self.axisRightMargin, dim.y - self.axisBottomMargin));
    self.svg
      .attr('width', dim.x)
      .attr('height', dim.y);
    self.xScale.domain([llUpperLeft.lng, llLowerRight.lng])
      .range([self.axisLeftMargin, dim.x - self.axisRightMargin]);
    self.yScale.domain([llUpperLeft.lat, llLowerRight.lat])
      .range([self.axisTopMargin, dim.y - self.axisBottomMargin]);

  }

  redraw(e) {
    const self = this;
    const map = e.target;
    const dim = map.getSize();
    self.updateScale(map);
    self.xAxis.scale(self.xScale);
    const p = map.containerPointToLayerPoint(new Point(0, 0));
    const transform = 'translate(' + p.x + ', ' + p.y + ')';
    self.svg.attr('transform', transform);
    self.svg.select('g.x-axis')
      .attr('transform', 'translate(0, ' + self.axisTopMargin + ')')
      .call(self.xAxis);
    self.svg.select('g.y-axis')
      .attr('transform', 'translate(' + (dim.x-self.axisRightMargin) + ', 0)')
      .call(self.yAxis);
  }
}

@Component({
  selector: 'ts-layer-coordinates',
  templateUrl: './layer-coordinates.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./layer-coordinates.component.css'],
})
export class LayerCoordinatesComponent implements OnInit {
  @Input() map: any;

  constructor() {
  }

  ngOnInit() {
    const self = this;


    self.map.addLayer(new LayerCoordinates(self.map));
  }

}
