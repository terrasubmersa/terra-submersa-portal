import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {MapLayerSelectorComponent} from '../map-layer-selector/map-layer-selector.component';

@Component({
  selector: 'ts-menu-actions',
  templateUrl: './menu-actions.component.html',
  styleUrls: ['./menu-actions.component.css']
})
export class MenuActionsComponent implements OnInit {

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  openMapLayerSelector() {
    this.dialog.open(MapLayerSelectorComponent, {
      position: {top: '67px', right: '5px'},
      width: '25em'
    })
    ;
  }
}
