import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MenuActionsComponent} from './menu-actions.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {importedModules} from '../../imported-modules';
import {APP_BASE_HREF} from '@angular/common';

describe('MenuActionsComponent', () => {
  let component: MenuActionsComponent;
  let fixture: ComponentFixture<MenuActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MenuActionsComponent],
      imports: importedModules,
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: APP_BASE_HREF, useValue : '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
