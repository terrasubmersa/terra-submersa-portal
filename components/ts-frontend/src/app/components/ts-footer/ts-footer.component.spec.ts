import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TsFooterComponent } from './ts-footer.component';
import {selectedMapLayersReducer} from '../../store/selected-map-layers-reducer';
import {StoreModule} from '@ngrx/store';
import {APP_BASE_HREF} from '@angular/common';

describe('TsFooterComponent', () => {
  let component: TsFooterComponent;
  let fixture: ComponentFixture<TsFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TsFooterComponent ],
      imports: [
        StoreModule.forRoot({
          selectedMapLayers: selectedMapLayersReducer
        })
        ],
      providers: [
        { provide: APP_BASE_HREF, useValue : '/' }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TsFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
