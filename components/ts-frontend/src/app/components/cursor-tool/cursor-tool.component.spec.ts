import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CursorToolComponent} from './cursor-tool.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {importedModules} from '../../imported-modules';
import {LatLng} from 'leaflet';
import {APP_BASE_HREF} from '@angular/common';
import {EventsService} from '../../services/events.service';

describe('CursorToolComponent', () => {
  let component: CursorToolComponent;
  let fixture: ComponentFixture<CursorToolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CursorToolComponent],
      imports: importedModules,
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        EventsService,
        {provide: APP_BASE_HREF, useValue: '/'}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CursorToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('formatLatLng', () => {
    it('NE', () => {
      const ll = new LatLng(12.34, 45.67);

      const str = component.formatLatLng(ll);

      expect(str).toEqual('12.34N 45.67E');
    });
    it('SW', () => {
      const ll = new LatLng(-12.34, -45.67);

      const str = component.formatLatLng(ll);

      expect(str).toEqual('12.34S 45.67W');
    });
    it('cut precision', () => {
      const ll = new LatLng(12.12345678, 45.12345678);

      const str = component.formatLatLng(ll);

      expect(str).toEqual('12.1235N 45.1235E');
    });
  })
});
