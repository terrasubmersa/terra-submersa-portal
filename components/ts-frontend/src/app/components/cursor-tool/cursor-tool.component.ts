import {Component, Input, OnInit} from '@angular/core';
import {Icon, LatLng, Layer, Marker, Polyline} from 'leaflet';
import {EventsService} from '../../services/events.service';
import {TextAnnotation} from '../../models/text-annotation';
import {LatLonCoords} from '../../models/lat-lon-coord';

const ACTION_SHOW_LOCATION = 'ACTION_SHOW_LOCATION';
const ACTION_MEASURE = 'ACTION_MEASURE';
const ACTION_ADD_TEXT_ANNOTATION = 'ACTION_ADD_TEXT_ANNOTATION';

@Component({
  selector: 'ts-cursor-tool',
  templateUrl: './cursor-tool.component.html',
  styleUrls: ['./cursor-tool.component.css']
})
export class CursorToolComponent implements OnInit {
  @Input() map: any;
  toolOutput = '';
  showDetails = false;

  // keep track of the cursor style to revert it
  origCursorStyle = undefined;
  // what is the undergoing action
  currentAction = undefined;
  // position origin and other stuff when measuring
  measureEnv;

  // we need to register the mous listener in order to remove them
  eventListeners = {};

  // icons, polylines and so that are added by the tools
  additionalLayers = [];

  // the icon for the measuring anchor
  iconMeasureAnchor = new Icon({
    iconUrl: 'assets/icons/ic_gps_fixed_black_24dp_2x.png',
    iconSize: [16, 16],
    iconAnchor: [8, 8]
  });

  buttons = [
    {
      angularticsAction: 'cursor-tool-click',
      angularticsLabel: 'show-location',
      fclick: () => this.buttonShowLocationClick(),
      icon: 'my_location',
      description: 'mouse coordinates'
    },
    {
      angularticsAction: 'cursor-tool-click',
      angularticsLabel: 'cursor-tool',
      fclick: () => this.buttonMeasureClick(),
      icon: 'straighten',
      description: 'measure distance'
    },
    {
      angularticsAction: 'cursor-tool-click',
      angularticsLabel: 'cursor-tool',
      fclick: () => this.buttonAddTextAnnotationeClick(),
      icon: 'border_color',
      description: 'add annotation'
    },

  ]


  constructor(private eventsService: EventsService) {
  }

  ngOnInit() {
    const self = this;
    if (self.map === undefined) {
      return;
    }
    self.origCursorStyle = self.map.getContainer().style.cursor;
  }

  buttonShowLocationClick() {
    const self = this;
    self.setCurrentAction(ACTION_SHOW_LOCATION)
  }

  buttonMeasureClick() {
    const self = this;
    self.setCurrentAction(ACTION_MEASURE)
  }

  buttonAddTextAnnotationeClick() {
    const self = this;
    self.setCurrentAction(ACTION_ADD_TEXT_ANNOTATION)
  }

  buttonCloseClick() {
    const self = this;
    self.setCurrentAction(undefined)
  }

  /**
   * set the undergoing action name
   * clears everything if action name is set to undefined
   * @param {string} actionName
   */
  setCurrentAction(actionName: string) {
    const self = this;
    if (!actionName) {
      self.currentAction = undefined;
      self.showDetails = false;
      self.clearEventListeners();
      self.map.getContainer().style.cursor = self.origCursorStyle;
      self.clearLayers();
      return;
    }
    self.clearLayers();
    self.clearMeasure();

    self.currentAction = actionName;
    self.showDetails = true;
    self.toolOutput = '';

    switch (actionName) {
      case ACTION_SHOW_LOCATION: {
        self.setEventListenerShowLocation();
        break;
      }
      case ACTION_MEASURE: {
        self.setEventListenerMeasure()
        break;
      }
      case ACTION_ADD_TEXT_ANNOTATION: {
        self.setEventListenerAddTextAnnotation()
        break;
      }
      default: {
        console.error('unregistered cursor action', actionName)
      }
    }
  }

  /**
   * transform LatLng in a readable string
   * @param {LatLng} ll
   * @return {string}
   */
  formatLatLng(ll: LatLng): string {
    const ns = (ll.lat > 0) ? 'N' : 'S';
    const ew = (ll.lng > 0) ? 'E' : 'W';

    const format = (n: number): number => Math.round(10000 * n) / 10000;
    return format(Math.abs(ll.lat)) + ns + ' ' + format(Math.abs(ll.lng)) + ew;
  }

  /**
   * format distance beteen two points, either in km or meters
   * @param {number} dist
   * @return {string}
   */
  formatDistance(dist: number): string {
    if (dist > 1000) {
      return (Math.round(dist / 100) / 10) + 'km';
    }
    return Math.round(dist) + 'm';
  }

  setEventListenerShowLocation() {
    const self = this;
    if (self.map === undefined) {
      return;
    }
    self.clearEventListeners();
    const f = function (event) {
      self.toolOutput = self.formatLatLng(event.latlng);
    };
    self.map.on('mousemove', f);
    self.eventListeners['mousemove'] = f;
    self.toolOutput = 'move the mouse';
    self.map.getContainer().style.cursor = 'crosshair';
  }

  /**
   * setup event listener dance in the case of measuring
   */
  setEventListenerMeasure() {
    const self = this;
    if (self.map === undefined) {
      return;
    }

    self.clearEventListeners();
    const fClick = function (event) {
      if (self.measureEnv) {
        self.clearMeasure();
        return;
      }
      self.measureEnv = {};
      self.addLayer(new Marker(event.latlng, {icon: self.iconMeasureAnchor}));
      self.measureEnv.polylineMeasure = new Polyline([event.latlng, event.latlng]);
      self.addLayer(self.measureEnv.polylineMeasure);
      self.measureEnv.latlngAnchor = event.latlng;
    };
    const fMove = function (event) {
      if (!self.measureEnv) {
        return;
      }
      self.measureEnv.polylineMeasure.setLatLngs([self.measureEnv.latlngAnchor, event.latlng]);
      self.toolOutput = self.formatDistance(self.measureEnv.latlngAnchor.distanceTo(event.latlng));
    };

    self.map.on('click', fClick);
    self.eventListeners['click'] = fClick;
    self.map.on('mousemove', fMove);
    self.eventListeners['mousemove'] = fMove;

    self.toolOutput = 'click & move';
    self.map.getContainer().style.cursor = 'crosshair';
  }

  /**
   * setup event listener dance in the case of measuring
   */
  setEventListenerAddTextAnnotation() {
    const self = this;
    if (self.map === undefined) {
      return;
    }

    self.clearEventListeners();
    const fClick = function (event) {
      const latlng = event.latlng;
      const ta = new TextAnnotation(
        undefined,
        '',
        new LatLonCoords(latlng.lat, latlng.lng),
        '',
        []
      );
      self.eventsService.focusTextAnnotation(ta);

      //self.eventsService.
    };

    self.map.on('click', fClick);
    self.eventListeners['click'] = fClick;
    self.map.getContainer().style.cursor = 'copy';
  }


  /**
   * clear the measuring transient data
   */
  clearMeasure() {
    const self = this;
    self.clearLayers();
    self.toolOutput = '';
    self.measureEnv = undefined;
  }

  /**
   * clear all events
   */
  clearEventListeners() {
    const self = this;

    for (const el in self.eventListeners) {
      self.map.off(el);
    }
    self.eventListeners = {};
  }

  /**
   * add a transient layer and register it for cleaning
   * @param {Layer} layer
   */
  addLayer(layer: Layer) {
    const self = this;
    layer.addTo(self.map);
    self.additionalLayers.push(layer)
  }

  /**
   * remove all tools layers (markers, lines...)
   */
  clearLayers() {
    const self = this;
    for (const m of self.additionalLayers) {
      self.map.removeLayer(m);
    }
  }
}


