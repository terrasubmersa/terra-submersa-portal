import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextAnnotationSelectorComponent } from './text-annotation-selector.component';
import {APP_BASE_HREF} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {importedModules} from '../../imported-modules';

describe('TextAnnotationSelectorComponent', () => {
  let component: TextAnnotationSelectorComponent;
  let fixture: ComponentFixture<TextAnnotationSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextAnnotationSelectorComponent ],
      imports: importedModules,
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'}
      ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextAnnotationSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
