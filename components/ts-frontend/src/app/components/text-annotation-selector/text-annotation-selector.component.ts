import {Component, OnInit} from '@angular/core';
import {AppState} from '../../AppState';
import {Store} from '@ngrx/store';
import * as TextAnnotationsActions from '../../store/text-annotations-actions';

@Component({
  selector: 'ts-text-annotation-selector',
  templateUrl: './text-annotation-selector.component.html',
  styleUrls: ['./text-annotation-selector.component.css']
})
export class TextAnnotationSelectorComponent implements OnInit {
  showAnnotation;

  constructor(private store: Store<AppState>) {
    this.showAnnotation = true;
  }

  ngOnInit() {
  }

  changeToggle(isChecked) {
    this.store.dispatch(new TextAnnotationsActions.SetTextAnnotationsVisibility(isChecked));
  }

}
