import {Component, Input, OnInit} from '@angular/core';
import {MapLayerDescription} from '../../models/map-layer-description';

@Component({
  selector: 'ts-tile-system-details',
  templateUrl: './tile-system-details.component.html',
  styleUrls: ['./tile-system-details.component.css']
})
export class TileSystemDetailsComponent implements OnInit {
  @Input() mapLayerDescription: MapLayerDescription;

  constructor() { }

  ngOnInit() {
  }

}
