import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TileSystemDetailsComponent} from './tile-system-details.component';
import {importedModules} from '../../imported-modules';
import {APP_BASE_HREF} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MapLayerDescription} from '../../models/map-layer-description';

describe('TileSystemDetailsComponent', () => {
  let component: TileSystemDetailsComponent;
  let fixture: ComponentFixture<TileSystemDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TileSystemDetailsComponent],
      imports: importedModules,
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileSystemDetailsComponent);
    component = fixture.componentInstance;
    component.mapLayerDescription = new MapLayerDescription('my_id', '', '', '', '', undefined)
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
