import {AvailableMapLayers} from './models/available-map-layers';
import {SelectedMapLayers} from './models/selected-map-layers';
import {TextAnnotationSet} from './models/text-annotation-set';

/**
 * the application state for the Redux Store
 */
export interface AppState {
  selectedMapLayers: SelectedMapLayers;
  mapLayers: string;
  textAnnotations: TextAnnotationSet;
}
