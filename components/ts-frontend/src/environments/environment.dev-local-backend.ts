// local properties to tie the angular aplciation to a local blowned Play backend
// ng serve --environment=dev-local-backend

export const environment = {
  production: false,
  apiOSM: '/files',
  urlTileBackend: '/tiles'
};
