export const environment = {
  production: true,
  apiOSM: '/tiles/files/osm-tiles',
  envName: 'production',
  urlTileBackend: '/tiles'
};
