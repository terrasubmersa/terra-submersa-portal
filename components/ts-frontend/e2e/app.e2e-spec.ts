import { TerraSubmersaFrontendPage } from './app.po';

describe('terra-submersa-frontend App', () => {
  let page: TerraSubmersaFrontendPage;

  beforeEach(() => {
    page = new TerraSubmersaFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toContain('Works');
  });
});
