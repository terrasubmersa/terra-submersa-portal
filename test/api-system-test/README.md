#API System test

## Development

### at once
Install at once a clean python. 
The point is not to use your default python that will polutte the requierements with tons of unwanted dependencies. 

    pip3 install virtualenv
    virtualenv ts-venv/
    #sets the correct paths
    . bin/activate
    
### Requirements
The libraries are stored into requirements.txt

#### Load the requirements saved into 
    pip install -r requirements.txt

#### Save the libraries you install in your env

    pip freeze > requirements.txt
    
### Run the test

Backend url is set via the `TS_BACKEND` environment variable 

At once 

    TS_BACKEND="http://localhost:9000" pytest src
    
Rerun test continuously on file changes, printing out console even for working tests:

    TS_BACKEND="http://localhost:9000" pytest-watch src/ -- -s
    
#### clean all remaining test data

    export TS_BACKEND="http://localhost:9000"
    for id in $(curl $TS_BACKEND/api/systems| jq . |grep '"id": '| grep '_system_test_' | cut -f4 -d'"')
    do 
      curl -X DELETE $TS_BACKEND/api/system/$id
      echo " delete"
    done