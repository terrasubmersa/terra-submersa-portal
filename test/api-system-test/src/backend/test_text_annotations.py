import unittest
import requests
from backend.TextAnnotationSubmission import TextAnnotationSubmission


class TestTextAnnotationSimple(unittest.TestCase):
    '''
    test case pushing data in the system
    '''

    @classmethod
    def setUpClass(cls):
        cls.submission = TextAnnotationSubmission()
        cls.test_annot = {
                       "text": "whatever",
                       "position": {"lat": 12.34, "lon": 56.78},
                       "tileSystemId": "ts-id",
                       "labels": ["one", "two"]
                       }

    def test_00_backend_is_defined(self):
        self.assertIsNotNone(self.submission.url_backend)

    def test_09_just_list_call(self):
        '''
        Just make a call to the system list to see if it answers
        '''
        req = requests.get(self.submission.url_backend + '/api/text-annotations')
        self.assertEqual(req.status_code, requests.codes.ok)

    def test_10_systems_can_be_read(self):
        '''
        check we can read the api and persists the number of initials systems
        '''
        all = self.submission.list()

        self.submission.count_init = self.submission.count(all)

        self.assertGreaterEqual(self.submission.count_init, 0)

    def test_40_add(self):
        data = self.test_annot

        req = requests.post(self.submission.url_backend + '/api/text-annotation', json=data)
        resp = req.json()

        self.assertEqual(req.status_code, requests.codes.ok)
        self.submission.tmp_id = resp['id']

    def test_60_remove(self):
        req = requests.delete(self.submission.url_backend + '/api/text-annotation/' + self.submission.tmp_id)

        self.assertEqual(req.status_code, requests.codes.ok)

    def test_70_should_not_find_sensys_system_anymore(self):
        t = self.submission.find_temp()

        self.assertIsNone(t)

if __name__ == '__main__':
    unittest.main()

