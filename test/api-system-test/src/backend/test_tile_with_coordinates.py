import unittest
import requests
from backend.TileSubmission import TileSubmission


class TestSensysSimple(unittest.TestCase):
    '''
    test case pushing data in the system
    '''
    text_title = 'with coordinates'
    text_description = 'test data'
    text_copyright = 'whatever copyright'

    @classmethod
    def setUpClass(cls):
        cls.submission = TileSubmission()
        cls.submission.copy_temp_resources()

    def test_00_backend_is_defined(self):
        self.assertIsNotNone(self.submission.url_backend)

    def test_40_add_system(self):
        files = {
            'file_image': open(self.submission.png, 'rb'),
        }
        data = {
            'id': self.submission.system_id,
            'coordinate_system': 'UTM34N',
            'lat_min': '4144320',
            'lat_max': '4144650',
            'lon_min': '688650',
            'lon_max': '688890',
            'text_title': self.text_title,
            'text_description': self.text_description,
            'text_copyright': self.text_copyright
        }

        req = requests.post(self.submission.url_backend + '/api/system',
                            files=files,
                            data=data
                            )
        resp = req.json()

        self.assertEqual(req.status_code, requests.codes.ok)
        self.assertEqual(resp['id'], self.submission.system_id)

    def test_50_should_find_system(self):
        t = self.submission.find_temp_system()

        self.assertIsNotNone(t)
        self.assertEqual(t['description'], self.text_description)
        self.assertEqual(t['copyright'], self.text_copyright)

    def test_60_remove(self):
        req = requests.delete(
            self.submission.url_backend + '/api/system/' + self.submission.system_id)

        self.assertEqual(req.status_code, requests.codes.ok)

    def test_70_add_system_with_error(self):
        files = {
            'file_image': open(self.submission.png, 'rb'),
            }
        data = {
            'id': self.submission.system_id,
            'coordinate_system': 'UTM34N',
            'XXXlat_min': '4144320',
            'lat_max': '4144650',
            'lon_min': '688650',
            'lon_max': '688890',
            'text_title': self.text_title,
            'text_description': self.text_description,
            'text_copyright': self.text_copyright
        }

        req = requests.post(self.submission.url_backend + '/api/system',
                            files=files,
                            data=data
                            )
        resp = req.json()

        self.assertNotEqual(req.status_code, requests.codes.ok)


if __name__ == '__main__':
    unittest.main()

