import unittest
import requests
from backend.SensysSubmission import SensysSubmission


class TestSensysMoreArgs(unittest.TestCase):
    '''
    test case pushing data in the system
    '''
    text_title = 'whatever title'
    text_description = 'test data - SensyMoreArgs'
    text_copyright = 'whatever copyright'

    @classmethod
    def setUpClass(cls):
        cls.sensys_submission = SensysSubmission()
        cls.sensys_submission.copy_temp_resources()

    def test_00_backend_is_defined(self):
        self.assertIsNotNone(self.sensys_submission.url_backend)

    def test_40_add_sensys_system(self):
        resp = self.sensys_submission.rex.add_sensys(self.sensys_submission.temp_sensys_png,
                                                     self.sensys_submission.temp_sensys_description,
                                                     title=self.text_title,
                                                     description=self.text_description,
                                                     copyright=self.text_copyright
                                                     )
        self.assertEqual(resp['id'], self.sensys_submission.sensys_system_id)

    def test_50_should_find_sensys_system(self):
        t = self.sensys_submission.find_temp_system()

        self.assertIsNotNone(t)
        self.assertEqual(t['description'], self.text_description)
        self.assertEqual(t['copyright'], self.text_copyright)

    def test_60_remove_sensys(self):
        self.sensys_submission.rex.remove(self.sensys_submission.sensys_system_id)


if __name__ == '__main__':
    unittest.main()
