import unittest
import requests
from backend.SensysSubmission import SensysSubmission

class TestSensysSimple(unittest.TestCase):
    '''
    test case pushing data in the system
    '''

    @classmethod
    def setUpClass(cls):
        cls.sensys_submission = SensysSubmission()
        cls.sensys_submission.copy_temp_resources()

    def test_00_backend_is_defined(self):
        self.assertIsNotNone(self.sensys_submission.url_backend)

    def test_09_just_list_call(self):
        '''
        Just make a call to the system list to see if it answers
        '''


        req = requests.get(self.sensys_submission.url_backend + '/api/systems')
        self.assertEqual(req.status_code, requests.codes.ok)

    def test_10_systems_can_be_read(self):
        '''
        check we can read the api and persists the number of initials systems
        '''
        systems = self.sensys_submission.list_systems()

        self.sensys_submission.systems_count_init = self.sensys_submission.count_systems(systems)

        self.assertGreaterEqual(self.sensys_submission.systems_count_init, 0)


    def test_30_should_not_find_sensys_system_yet(self):
        t = self.sensys_submission.find_temp_system()

        self.assertIsNone(t)

    def test_31_should_not_find_sensys_tile_png_yet(self):
        req = self.sensys_submission.request_one_tile()

        self.assertNotEqual(req.status_code, requests.codes.ok)


    def test_40_add_sensys_system(self):
        resp = self.sensys_submission.rex.add_sensys(self.sensys_submission.temp_sensys_png, self.sensys_submission.temp_sensys_description)
        self.assertEqual(resp['id'], self.sensys_submission.sensys_system_id)

    def test_50_should_find_sensys_system(self):
        t = self.sensys_submission.find_temp_system()

        self.assertIsNotNone(t)

    def test_51_should_not_find_sensys_tile_png_yet(self):
        req = self.sensys_submission.request_one_tile()

        self.assertEqual(req.status_code, requests.codes.ok)


    def test_60_remove_sensys(self):
        self.sensys_submission.rex.remove(self.sensys_submission.sensys_system_id)

    def test_70_should_not_find_sensys_system_anymore(self):
        t = self.sensys_submission.find_temp_system()

        self.assertIsNone(t)

    def test_71_should_not_find_sensys_tile_png_yet(self):
        req = self.sensys_submission.request_one_tile()

        self.assertNotEqual(req.status_code, requests.codes.ok)

if __name__ == '__main__':
    unittest.main()
