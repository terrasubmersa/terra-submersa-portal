import os
import random
import requests
import tempfile
import shutil

class TileSubmission:
    url_backend = os.environ['TS_BACKEND']

    systems_count_init = None
    system_id = None
    png = None
    tile_path = '/17/73958/50822.png'

    def __init__(self):
        self.rnd_tag = '_system_test_' + str(int(random.uniform(0, 1000000000)))
        self.temp_dir = tempfile.TemporaryDirectory()
        self.file_in_root = 'with-coordinates-test'
        self.file_out_root = '_tile_system_' + self.rnd_tag

        self.system_id = 'with-coordinates-' + self.file_out_root
        self.png = self.temp_dir.name + '/' + self.file_out_root + '.png'

    def copy_temp_resources(self):
        shutil.copy('resources/' + self.file_in_root + '.png', self.png)


    def count_systems(self, systems):
        return len(systems)

    def list_systems(self):
        r = requests.get(self.url_backend + '/api/systems')
        return r.json()

    def request_one_tile(self):
        return requests.get(self.url_backend +'/files/osm-tiles/images/' + self.system_id + self.tile_path)

    def find_system(self, id):

        xs = [x for x in self.list_systems() if x['id'] == id]
        if not xs:
            return None
        else:
            return xs[0]

    def find_temp_system(self):
        return self.find_system(self.system_id)
