import os
import random
import requests
import tempfile
import shutil

class TextAnnotationSubmission:
    url_backend = os.environ['TS_BACKEND']

    count_init = None
    tmp_id = None

    def __init__(self):
        pass

    def count(self, systems):
        return len(systems)

    def list(self):
        print(self.url_backend + '/api/text-annotations\n')
        r = requests.get(self.url_backend + '/api/text-annotations')
        return r.json()

    def find(self, id):

        xs = [x for x in self.list() if x['id'] == id]
        if not xs:
            return None
        else:
            return xs[0]

    def find_temp(self):
        return self.find(self.tmp_id)