import os
import random
import requests
import tempfile
import shutil
from ts_cli.rest_cli import RestCli
from ts_cli.tiles.cli_tiles_exec import CLITilesExec

class SensysSubmission:
    url_backend = os.environ['TS_BACKEND']
    rest_cli = RestCli(url_backend)
    rex = CLITilesExec(url_backend)

    systems_count_init = None
    sensys_system_id = None
    temp_sensys_png = None
    temp_sensys_description = None
    sensys_tile_path = '/17/73958/50822.png'

    def __init__(self):
        self.rnd_tag = '_system_test_' + str(int(random.uniform(0, 1000000000)))
        self.temp_dir = tempfile.TemporaryDirectory()
        self.file_in_root = 'sensys-test'
        self.file_out_root = '_tile_system_' + self.rnd_tag

        self.sensys_system_id = 'sensys-' + self.file_out_root
        self.temp_sensys_png = self.temp_dir.name + '/' + self.file_out_root + '.png'
        self.temp_sensys_description = self.temp_dir.name + '/' + self.file_out_root + '.tif.txt'

    def copy_temp_resources(self):
        shutil.copy('resources/' + self.file_in_root + '.png', self.temp_sensys_png)
        shutil.copy('resources/' + self.file_in_root + '.tif.txt', self.temp_sensys_description)


    def count_systems(self, systems):
        return len(systems)

    def list_systems(self):
        return self.rex.list('id')

    def request_one_tile(self):
        return requests.get(self.url_backend +'/files/osm-tiles/' + self.sensys_system_id + self.sensys_tile_path)

    def find_system(self, id):

        xs = [x for x in self.list_systems() if x['id'] == id]
        if not xs:
            return None
        else:
            return xs[0]

    def find_temp_system(self):
        return self.find_system(self.sensys_system_id)
